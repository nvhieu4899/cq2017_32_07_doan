package com.ygaps.travelapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notification {
    @SerializedName("lat")
    @Expose
    private double latitude;
    @SerializedName("long")
    @Expose
    private double longtitude;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("notificationType")
    @Expose
    private int notiType;
    @SerializedName("speed")
    @Expose
    private int speed;
    @SerializedName("createdByTourId")
    @Expose
    private int tourId;

}
