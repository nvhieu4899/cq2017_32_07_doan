package com.ygaps.travelapp.TourActivities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.ygaps.travelapp.MyApplication;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.data.model.Tours;
import com.ygaps.travelapp.data.model.UserSearchResult;
import com.ygaps.travelapp.retrofit.RetrofitClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchAndInviteUser extends AppCompatActivity {
  private RecyclerView recyclerView;
  private SearchView searchView;
  private ArrayList<UserSearchResult.UserSearch> userSearches = new ArrayList<>();
  private SearchResultAdapter searchResultAdapter;
  final private int PAGE_SIZE = 10;
  private int tourId;
  private int total;
  private int loadedPage;
  private boolean isLoading = false;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_search_and_invite_user);

    tourId = getIntent().getIntExtra("tourId", -1);


    recyclerView = (RecyclerView) findViewById(R.id.recycler);

    recyclerView.setHasFixedSize(true);

    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
    recyclerView.setLayoutManager(linearLayoutManager);
    searchResultAdapter = new SearchResultAdapter();
    recyclerView.setAdapter(searchResultAdapter);
    searchView = (SearchView) findViewById(R.id.search);
    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
      @Override
      public boolean onQueryTextSubmit(String query) {
        Call call = RetrofitClient.getInstance().getApi().searchUser(query, 1, PAGE_SIZE);
        call.enqueue(new Callback<UserSearchResult>() {
          @Override
          public void onResponse(Call<UserSearchResult> call, Response<UserSearchResult> response) {
            if (response.isSuccessful()) {
              userSearches.clear();
              userSearches.addAll(response.body().getUserSearches());
              searchResultAdapter.notifyDataSetChanged();
              total = response.body().getTotal();
              loadedPage = 1;
            }
          }

          @Override
          public void onFailure(Call<UserSearchResult> call, Throwable t) {

          }
        });
        return false;
      }

      @Override
      public boolean onQueryTextChange(String newText) {
        return false;
      }
    });
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
      }

      @Override
      public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (!isLoading) {
          if (linearLayoutManager != null && linearLayoutManager.findLastVisibleItemPosition() == userSearches.size() - 1) {
            loadMoreSearchResult();
            isLoading = true;
          }
        }
      }
    });
  }


  void loadMoreSearchResult()
  {
    Handler handler = new Handler();
    handler.postDelayed(new Runnable() {
      @Override
      public void run() {
        if (userSearches.size() != total) {
          Call call = RetrofitClient.getInstance().getApi().searchUser(searchView.getQuery().toString(), loadedPage+1, PAGE_SIZE);
          call.enqueue(new Callback<UserSearchResult>() {
            @Override
            public void onResponse(Call<UserSearchResult> call, Response<UserSearchResult> response) {
              if (response.isSuccessful()) {
                userSearches.addAll(response.body().getUserSearches());
                searchResultAdapter.notifyDataSetChanged();
                loadedPage++;
              }
              isLoading = false;
            }

            @Override
            public void onFailure(Call<UserSearchResult> call, Throwable t) {
              isLoading = false;
            }
          });
        }
      }
    }, 2000);
  }

  public class SearchResultAdapter extends RecyclerView.Adapter<MyViewHolder> {
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
      View view = layoutInflater.inflate(R.layout.recycler_item_search_user, parent, false);
      return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
      Picasso.get().load(userSearches.get(position).getAvatar()).error(R.drawable.img_default).into(holder.avatar);
      holder.name.setText(userSearches.get(position).getFullName());
      holder.email.setText(userSearches.get(position).getEmail());
      holder.phone.setText(userSearches.get(position).getPhone());
      holder.invite.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          Call call = RetrofitClient
                    .getInstance()
                    .getApi()
                    .addMemberToTour(MyApplication.token, String.valueOf(tourId), String.valueOf(userSearches.get(position).getId()), false);
          call.enqueue(new Callback<ResponseBody>() {
              @Override
              public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                  holder.invite.setEnabled(false);
                  Toast.makeText(getApplicationContext(), R.string.success_invite, Toast.LENGTH_LONG).show();
                } else {
                  try {
                    Toast.makeText(getApplicationContext(), response.errorBody().string(), Toast.LENGTH_LONG).show();
                  } catch (IOException e) {
                    e.printStackTrace();
                  }
                }
              }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
          });
        }
      });
    }

    @Override
    public int getItemCount() {
      return userSearches.size();
    }


  }


  public class MyViewHolder extends RecyclerView.ViewHolder {
    private CircleImageView avatar;
    private TextView name;
    private TextView email;
    private TextView phone;
    private Button invite;

    public MyViewHolder(@NonNull View itemView) {
      super(itemView);
      avatar = itemView.findViewById(R.id.avatar);
      name = itemView.findViewById(R.id.name);
      email = itemView.findViewById(R.id.email);
      phone = itemView.findViewById(R.id.phone);
      invite = itemView.findViewById(R.id.invite);
    }
  }
}
