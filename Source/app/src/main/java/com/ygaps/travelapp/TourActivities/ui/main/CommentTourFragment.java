package com.ygaps.travelapp.TourActivities.ui.main;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.data.model.TourInfo;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class CommentTourFragment extends Fragment {
  // TODO: Rename parameter arguments, choose names that match
  // the fragment initialization parameters, e.g. ARG_ITEM_NUMBE

  public CommentTourFragment() {
    // Required empty public constructor
  }
  private PageViewModel pageViewModel;
  private ArrayList<TourInfo.Comment> comments = new ArrayList<>();
  private RecyclerView recyclerView;
  private CommentsAdapter commentsAdapter;

  // TODO: Rename and change types and number of parameters
  public static CommentTourFragment newInstance() {
    CommentTourFragment fragment = new CommentTourFragment();

    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    pageViewModel = ViewModelProviders.of(getActivity()).get(PageViewModel.class);

  }

  @Override
  public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment


    View view = inflater.inflate(R.layout.fragment_comment_tour, container, false);

    recyclerView = (RecyclerView)view.findViewById(R.id.recycler);
    recyclerView.setHasFixedSize(true);


    commentsAdapter= new CommentsAdapter();

    recyclerView.setAdapter(commentsAdapter);
    pageViewModel.getObserveable().observe(getViewLifecycleOwner(), new Observer<TourInfo>() {
      @Override
      public void onChanged(TourInfo tourInfo) {
        if (tourInfo!=null)
        {
          comments = tourInfo.getComments();
          commentsAdapter.notifyDataSetChanged();
        }
      }
    });

  return view;

  }
  public class CommentsAdapter extends RecyclerView.Adapter<CustomViewHolder>
  {
    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      LayoutInflater inflater = LayoutInflater.from(getContext());
      View view = inflater.inflate(R.layout.recycler_comments, parent, false);
      return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
      Picasso.get().load(comments.get(position).getAvatar()).error(R.drawable.img_default).into(holder.avatar);
      holder.name.setText(comments.get(position).getName());
      holder.comment.setText(comments.get(position).getComment());
    }

    @Override
    public int getItemCount() {
      return 0;
    }
  }


  public class CustomViewHolder extends RecyclerView.ViewHolder
  {
    private CircleImageView avatar;
    private TextView name;
    private TextView comment;
    public CustomViewHolder(@NonNull View itemView) {
      super(itemView);
      avatar = (CircleImageView)itemView.findViewById(R.id.avatar);
      name = (TextView)itemView.findViewById(R.id.name);
      comment = (TextView)itemView.findViewById(R.id.comment);
    }
  }

}
