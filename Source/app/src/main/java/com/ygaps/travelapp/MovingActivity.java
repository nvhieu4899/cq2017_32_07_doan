package com.ygaps.travelapp;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.base.Strings;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.messaging.FirebaseMessaging;
import com.ygaps.travelapp.Services.LocationSender;
import com.ygaps.travelapp.TourActivities.AddStopPointActivity;
import com.ygaps.travelapp.TourActivities.ManageTourHistory;
import com.ygaps.travelapp.TourActivities.ui.main.PageViewModel;
import com.ygaps.travelapp.data.model.MemberCoordinate;
import com.ygaps.travelapp.data.model.NotiList;
import com.ygaps.travelapp.data.model.NotiOnRoad;
import com.ygaps.travelapp.data.model.StopPoint;
import com.ygaps.travelapp.data.model.TourInfo;
import com.ygaps.travelapp.retrofit.RetrofitClient;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.ygaps.travelapp.Nav_Fragment.MapFragment.MY_PERMISSIONS_REQUEST_LOCATION;
import static com.ygaps.travelapp.TourActivities.PickSourceAndDestination.bitmapDescriptorFromVector;

public class MovingActivity extends FragmentActivity implements OnMapReadyCallback{

    private FusedLocationProviderClient fusedLocationClient;

    private GoogleMap mMap;
    private ImageButton btn_police;
    private ImageButton btn_warning;
    private ImageButton btn_speedlimit50;
    private ImageButton btn_speedlimit80;
    private ImageButton btn_micro_on;
    private int tourId;
    private String note = "";
    private Polyline polyline;
    String pathSave = "";

    MediaRecorder mediaRecorder;
    MediaPlayer mediaPlayer;
    String nameRecorderRandom = "";
    final int REQUEST_PERMISSION_CODE = 1000;


    private Handler otherMembersHandler = new Handler();

    private TourInfo tourInfo;

    private HashMap<Integer, Integer> spinnerMap = new HashMap<>();

    private Spinner spinner;
    private String currentTrackingUser = MyApplication.userId;
    private HashMap<String,Marker> memberMarkers = new HashMap<>();

    private ArrayList<NotiOnRoad> notiOnRoads = new ArrayList<>();
    private ArrayList<Marker> notiOnRoadMarkers = new ArrayList<>();
    private NotificationManager notificationManager;


    private ToggleButton toggleButton;
    private boolean isTracking= true;
    private boolean started = true;
    private Runnable otherMembersRunnable = new Runnable() {
        @Override
        public void run() {
            if (started)
            {
                fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                      if (location == null)
                      {
                        Toast.makeText(getApplicationContext(),R.string.require_GPS_moving,Toast.LENGTH_LONG).show();
                        started = false;
                        return;
                      }
                        Call call = RetrofitClient
                                .getInstance()
                                .getApi()
                                .getCoordinateMember(MyApplication.token
                                        ,MyApplication.userId
                                        ,String.valueOf(tourId)
                                        ,location.getLatitude()
                                        ,location.getLongitude());
                        call.enqueue(new Callback<ArrayList<MemberCoordinate>>()
                        {
                            @Override
                            public void onResponse(Call<ArrayList<MemberCoordinate>> call, Response<ArrayList<MemberCoordinate>> response) {
                                if (response.isSuccessful())
                                {
                                    for (MemberCoordinate memberCoordinate : response.body())
                                    {
                                        if (!memberMarkers.containsKey(memberCoordinate.getId()))
                                        {
                                            Marker marker = mMap.addMarker(new MarkerOptions()
                                                    .position(new LatLng(Double.valueOf(memberCoordinate.getLat()),Double.valueOf(memberCoordinate.get_long())))
                                                    .draggable(false)
                                                    .icon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_car))
                                                    .anchor(0.5f,0.5f));
                                            memberMarkers.put(memberCoordinate.getId(),marker);
                                        }
                                        else
                                        {
                                            Marker member = memberMarkers.get(memberCoordinate.getId());
                                            member.setPosition(new LatLng(Double.valueOf(memberCoordinate.getLat()),Double.valueOf(memberCoordinate.get_long())));
                                        }
                                        if (isTracking) {
                                            if (!memberMarkers.containsKey(currentTrackingUser))
                                                currentTrackingUser = MyApplication.userId;
                                            Marker trackingMarker = memberMarkers.get(currentTrackingUser);
                                            mMap.moveCamera(CameraUpdateFactory.newLatLng(trackingMarker.getPosition()));
                                        }
                                    }

                                }

                            }

                            @Override
                            public void onFailure(Call<ArrayList<MemberCoordinate>> call, Throwable t) {

                            }
                        });
                    }
                });
            }
        }
    };
    private Runnable getNotiOnRoad = new Runnable() {
        @Override
        public void run() {
            if (started)
            {
                fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                      if (location == null)
                      {
                        Toast.makeText(getApplicationContext(),R.string.require_GPS_moving,Toast.LENGTH_LONG).show();
                        started = false;

                        return;
                      }
                        Call call = RetrofitClient.getInstance().getApi().getNotiOnRoadByCoordinate(MyApplication.token,location.getLatitude(),location.getLongitude(),1);
                        call.enqueue(new Callback<NotiList>(){
                            @Override
                            public void onResponse(Call<NotiList> call, Response<NotiList> response) {
                                if (response.isSuccessful()) {
                                  for (Marker marker: notiOnRoadMarkers)
                                  {
                                    marker.remove();
                                  }
                                  notiOnRoadMarkers.clear();
                                    notiOnRoads.clear();
                                    notiOnRoads.addAll(response.body().getNotiOnRoads());
                                    for (NotiOnRoad notiOnRoad : notiOnRoads)
                                    {

                                            LatLng latLng = new LatLng(Double.parseDouble(notiOnRoad.getLatitude()), Double.parseDouble(notiOnRoad.getLongtitude()));
                                            Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).draggable(false));
                                            if (notiOnRoad.getNotiType()==1)
                                            {
                                                marker.setIcon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_police));
                                                marker.setTitle(note);
                                            }
                                            else if (notiOnRoad.getNotiType()==2)
                                            {
                                                marker.setIcon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_warning));
                                                marker.setTitle(note);
                                            }
                                            else if (notiOnRoad.getNotiType()==3)
                                            {
                                                marker.setIcon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_speed_limit_2));
                                                marker.setTitle(getString(R.string.speed_limit_title)+String.valueOf(notiOnRoad.getSpeed())+" "+getString(R.string.note)+": "+notiOnRoad.getNote());
                                            }
                                            notiOnRoadMarkers.add(marker);
                                    }
                                }

                            }

                            @Override
                            public void onFailure(Call<NotiList> call, Throwable t) {

                            }
                        });
                    }
                });
            }
        }
    };
  private Thread thread = new Thread()
  {

    @Override
    public void run() {
      while (started)
      {
        try {
          Thread.sleep(5000);
        } catch (InterruptedException e)
        {
          e.printStackTrace();
        }
        runOnUiThread(otherMembersRunnable);
        runOnUiThread(getNotiOnRoad);

      }
      if (!started) finish();
    }
  };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stopService(new Intent(this,LocationSender.class));
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(1111);
        setContentView(R.layout.activity_moving);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        //subscribe topic
        toggleButton = findViewById(R.id.isTracking);
        toggleButton.setChecked(true);
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isTracking=isChecked;
                if (isChecked)
                {
                    if (!memberMarkers.containsKey(currentTrackingUser))
                        currentTrackingUser = MyApplication.userId;
                    Marker trackingMarker = memberMarkers.get(currentTrackingUser);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(trackingMarker.getPosition()));
                }
            }
        });
        Intent intent = getIntent();
        tourId = intent.getIntExtra("tourId",1);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        Call call = RetrofitClient.getInstance().getApi().getTourInfo(MyApplication.token,tourId);
        call.enqueue(new Callback<TourInfo>()
        {
            @Override
            public void onResponse(Call<TourInfo> call, Response<TourInfo> response) {
                if (response.isSuccessful()) tourInfo = response.body();
                ArrayList<TourInfo.Member> members = tourInfo.getMembers();
                String[] spinnerArray = new String[tourInfo.getMembers().size()];
                for (int i = 0; i < tourInfo.getMembers().size(); i++)
                {
                    spinnerMap.put(i,members.get(i).getId());
                    spinnerArray[i] = members.get(i).getName();
                }
                spinner = findViewById(R.id.member);
                ArrayAdapter<String> adapter =new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item, spinnerArray);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        currentTrackingUser = String.valueOf(spinnerMap.get(spinner.getSelectedItemPosition()));
                        if (!memberMarkers.containsKey(currentTrackingUser)) currentTrackingUser = MyApplication.userId;
                        Marker trackingMarker = memberMarkers.get(currentTrackingUser);
                        if (trackingMarker!=null)
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(trackingMarker.getPosition()));
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onFailure(Call<TourInfo> call, Throwable t) {

            }
        });

        FirebaseMessaging.getInstance().subscribeToTopic("/topics/tour-id-"+String.valueOf(tourId));

        thread.start();
        btn_police=(ImageButton)findViewById(R.id.police_station);
        btn_warning=(ImageButton)findViewById(R.id.problem_on_road);
        btn_speedlimit50=(ImageButton)findViewById(R.id.speed_limit_50);
        btn_speedlimit80=(ImageButton)findViewById(R.id.speed_limit_80);
        btn_micro_on = findViewById(R.id.microphone_on);
        btn_police.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MovingActivity.this);
                builder.setTitle(getString(R.string.send_noti_police));


                final EditText input = new EditText(getApplicationContext());
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                input.setMaxLines(1);
                input.setHint(R.string.note);

                builder.setView(input);

                builder.setPositiveButton(getString(R.string.btn_send), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        note = input.getText().toString();
                        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(MovingActivity.this);
                        client.getLastLocation().addOnSuccessListener(MovingActivity.this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                Call call = RetrofitClient.getInstance().getApi().createNotiOnRoad(MyApplication.token,
                                        location.getLatitude(),
                                        location.getLongitude(),
                                        tourId,Integer.valueOf(MyApplication.userId),
                                        2,null,note
                                );
                                call.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        if (response.isSuccessful())
                                        {
                                            Toast.makeText(MovingActivity.this,R.string.success_create_noti,Toast.LENGTH_LONG).show();
                                        }
                                        else
                                        {
                                            try {
                                                Toast.makeText(MovingActivity.this,response.errorBody().string(),Toast.LENGTH_LONG).show();
                                            }catch (IOException e)
                                            {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                    @Override
                                    public void onFailure(Call call, Throwable t) {
                                        Toast.makeText(MovingActivity.this,"That bai",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });

                    }
                });
                builder.setNegativeButton(getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
        btn_warning.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MovingActivity.this);
                builder.setTitle(getString(R.string.send_noti_problems));
                final EditText input = new EditText(getApplicationContext());
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                input.setMaxLines(1);
                input.setHint(R.string.note);
                builder.setView(input);
                builder.setPositiveButton(getString(R.string.btn_send), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        note = input.getText().toString();
                        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(MovingActivity.this);
                        client.getLastLocation().addOnSuccessListener(MovingActivity.this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                Call call = RetrofitClient.getInstance().getApi().createNotiOnRoad(MyApplication.token,
                                        location.getLatitude(),
                                        location.getLongitude(),
                                        tourId,Integer.valueOf(MyApplication.userId),
                                        2,null,note
                                );
                                call.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        if (response.isSuccessful()) {
                                          Toast.makeText(MovingActivity.this,R.string.success_create_noti,Toast.LENGTH_LONG).show();

                                        }
                                        else
                                        {
                                            try {
                                                Toast.makeText(MovingActivity.this,response.errorBody().string(),Toast.LENGTH_LONG).show();
                                            }catch (IOException e)
                                            {
                                                e.printStackTrace();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call call, Throwable t) {
                                    }
                                });
                            }
                        });

                    }
                });
                builder.setNegativeButton(getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
        btn_speedlimit50.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MovingActivity.this);
                builder.setTitle(getString(R.string.send_noti_speed50));


                final EditText input = new EditText(getApplicationContext());
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                input.setMaxLines(1);
                input.setHint(R.string.note);

                builder.setView(input);

                builder.setPositiveButton(getString(R.string.btn_send), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        note = input.getText().toString();
                        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(MovingActivity.this);
                        client.getLastLocation().addOnSuccessListener(MovingActivity.this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                Call call = RetrofitClient.getInstance().getApi().createNotiOnRoad(MyApplication.token,
                                        location.getLatitude(),
                                        location.getLongitude(),
                                        tourId,Integer.valueOf(MyApplication.userId),
                                        3,50,note);
                                call.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        if (response.isSuccessful())
                                        {
                                          Toast.makeText(MovingActivity.this,R.string.success_create_noti,Toast.LENGTH_LONG).show();

                                        }
                                        else
                                        {
                                            try {
                                                Toast.makeText(MovingActivity.this,response.errorBody().string(),Toast.LENGTH_LONG).show();
                                            }catch (IOException e)
                                            {
                                                e.printStackTrace();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call call, Throwable t) {
                                    }
                                });
                            }
                        });

                    }
                });
                builder.setNegativeButton(getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
        btn_speedlimit80.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MovingActivity.this);
                builder.setTitle(getString(R.string.send_noti_speed80));


                final EditText input = new EditText(getApplicationContext());
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                input.setMaxLines(1);
                input.setHint(R.string.note);
                builder.setView(input);
                builder.setPositiveButton(getString(R.string.btn_send), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        note = input.getText().toString();
                        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(MovingActivity.this);
                        client.getLastLocation().addOnSuccessListener(MovingActivity.this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                Call call = RetrofitClient.getInstance().getApi().createNotiOnRoad(MyApplication.token,
                                        location.getLatitude(),
                                        location.getLongitude(),
                                        tourId,Integer.valueOf(MyApplication.userId),
                                        3,80,note
                                );
                                call.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        if (response.isSuccessful()) {
                                            try {
                                                Toast.makeText(MovingActivity.this, response.body().string(), Toast.LENGTH_LONG).show();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        else {
                                            try {
                                                Toast.makeText(MovingActivity.this, response.errorBody().string(), Toast.LENGTH_LONG).show();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                    @Override
                                    public void onFailure(Call call, Throwable t) {
                                    }
                                });
                            }
                        });
                    }
                });
                builder.setNegativeButton(getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        if(!checkPermissionFromDevice()) {
            requestPermission();
        }

        final int[] count = {0};
        btn_micro_on.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                count[0]++;
                if(count[0]%2 == 1) {
                    if(checkPermissionFromDevice()) {
                        nameRecorderRandom = UUID.randomUUID().toString();
                        pathSave = Environment.getExternalStorageDirectory()
                                .getAbsolutePath() + "/"
                                + nameRecorderRandom + "_audio_record.3gp";
                        setupMediaRecorder();
                        try {
                            mediaRecorder.prepare();
                            mediaRecorder.start();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(MovingActivity.this, "Đang ghi âm", Toast.LENGTH_SHORT).show();

                        btn_micro_on.setImageResource(R.drawable.ic_mic_off_black_24dp);
                    }
                    else {
                        requestPermission();
                    }
                }
                else {
                    mediaRecorder.stop();
                    Toast.makeText(MovingActivity.this, "Đã dừng ghi âm", Toast.LENGTH_SHORT).show();

                    mediaPlayer = new MediaPlayer();
                    try {
                        mediaPlayer.setDataSource(pathSave);
                        mediaPlayer.prepare();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mediaPlayer.start();
                    Toast.makeText(MovingActivity.this, "Đang tự phát lại", Toast.LENGTH_SHORT).show();

                    btn_micro_on.setImageResource(R.drawable.ic_mic_none_black_24dp);
                }

            }
        });

    }
    private void setupMediaRecorder() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(pathSave);
    }
    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO

        }, REQUEST_PERMISSION_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Đã cấp phép quyền truy cập", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(this, "Đã từ chối cấp quyền", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private boolean checkPermissionFromDevice() {
        int write_external_storage_result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int record_audio_result = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        return write_external_storage_result == PackageManager.PERMISSION_GRANTED &&
                record_audio_result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (checkLocationPermission())
        {
            mMap.setMyLocationEnabled(true);
        }
        else
        {
            Toast.makeText(getApplicationContext(),R.string.require_GPS_moving,Toast.LENGTH_LONG).show();
            finish();
        }
        Call call = RetrofitClient.getInstance().getApi().getTourInfo(MyApplication.token,tourId);
        call.enqueue(new Callback<TourInfo>(){
            @Override
            public void onResponse(Call<TourInfo> call, Response<TourInfo> response) {

                if (response.isSuccessful())
                {
                    createStopPointsOnMap(response.body().getStopPoints());
                }
            }
            @Override
            public void onFailure(Call<TourInfo> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onPause() {
        thread.interrupt();
        super.onPause();
    }


    private boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(MovingActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getParent(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle(R.string.title_location_permission)
                        .setMessage(R.string.text_location_permission)
                        .setPositiveButton(R.string.btn_sure, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(getParent(),
                                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getParent(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }
    private void addSPMarker(StopPoint sp)
    {
        Marker marker;
        if (sp.getServiceTypeId()==1)
            marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(sp.getLatitude()),Double.parseDouble(sp.getLongtitude())))
                    .title(sp.getName())
                    .draggable(false).icon(
                            bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_restaurant)
                    ).anchor(0.5f,0.5f));
        else if (sp.getServiceTypeId()==2)
            marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(sp.getLatitude()),Double.parseDouble(sp.getLongtitude())))
                    .title(sp.getName())
                    .draggable(false).icon(
                            bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_hotel)
                    ).anchor(0.5f,0.5f));
        else if (sp.getServiceTypeId()==3)
            marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(sp.getLatitude()),Double.parseDouble(sp.getLongtitude())))
                    .title(sp.getName())
                    .draggable(false).icon(
                            bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_fuel)
                    ).anchor(0.5f,0.5f));
        else
            marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(sp.getLatitude()),Double.parseDouble(sp.getLongtitude())))
                    .title(sp.getName())
                    .draggable(false).icon(
                            bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_pin)
                    ));
        marker.setTag(sp);
    }
    public void createStopPointsOnMap(List<StopPoint> stopPoints) {
        if (stopPoints.size()==0) return;
        for(int i = 0; i< stopPoints.size(); i++) {
            addSPMarker(stopPoints.get(i));
        }
        double first_latStopPoint = Double.parseDouble(stopPoints.get(0).getLatitude());
        double first_longStopPoint = Double.parseDouble(stopPoints.get(0).getLongtitude());
        LatLng firstStopPoint = new LatLng(first_latStopPoint, first_longStopPoint);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(firstStopPoint));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker.getTag()!=null) {
                    final StopPoint stopPoint = (StopPoint) marker.getTag();
                    AlertDialog.Builder builder = new AlertDialog.Builder(MovingActivity.this);
                    builder.setTitle(stopPoint.getName());
                    builder.setMessage("Bạn có muốn đi tới đây?");
                    builder.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    if (polyline!=null)
                                    polyline.remove();
                                    polyline = mMap.addPolyline(new PolylineOptions()
                                            .add(new LatLng(location.getLatitude(),location.getLongitude())
                                                    , new LatLng(Double.parseDouble(stopPoint.getLatitude()),Double.parseDouble(stopPoint.getLongtitude())))
                                            .color(Color.BLUE)
                                            .width(5));
                                }
                            });
                        }
                    });
                    builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.quit_tracking_message));

        builder.setPositiveButton(getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(getApplicationContext(),LocationSender.class);
                intent.putExtra("tourId",String.valueOf(tourId));
                startService(intent);
                String channelId = getString(R.string.project_id);
                NotificationCompat.Builder builder1 = new NotificationCompat.Builder(getApplicationContext(),channelId);
                if (tourInfo!=null)
                builder1.setContentText("Bạn đang theo dõi tour "+tourInfo.getName())
                        .setPriority(NotificationCompat.PRIORITY_MAX);
                builder1.setSmallIcon(R.drawable.ic_icons);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel channel = new NotificationChannel(
                            channelId,
                            "Channel human readable title",
                            NotificationManager.IMPORTANCE_HIGH);
                    notificationManager.createNotificationChannel(channel);
                }

                Intent viewInfoIntent = new Intent(getApplicationContext(), MovingActivity.class);
                viewInfoIntent.putExtra("tourId",tourId);
                viewInfoIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent_view = PendingIntent.getActivity(getApplicationContext(), 0, viewInfoIntent, PendingIntent.FLAG_UPDATE_CURRENT);


                builder1.setContentIntent(pendingIntent_view);
                notificationManager.notify(1111, builder1.build());


                MovingActivity.super.onBackPressed();
            }
        });
        builder.setNegativeButton(getString(R.string.btn_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/tour-id-"+String.valueOf(tourId));
                MovingActivity.super.onBackPressed();
            }
        });
        builder.setNeutralButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
