package com.ygaps.travelapp.Firebase;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import com.ygaps.travelapp.Main;
import com.ygaps.travelapp.MyApplication;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.ReceiveNotiButtonAction;
import com.ygaps.travelapp.TourActivities.ManageTourHistory;
import com.ygaps.travelapp.retrofit.RetrofitClient;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FirebaseMessage extends FirebaseMessagingService {
    public FirebaseMessage() {
    }
    final public String TAG = "Du lich cho vui firebase:";
    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {

        super.onMessageReceived(remoteMessage);
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            String messageType = remoteMessage.getData().get("type");
            if (messageType.contentEquals("6"))
            sendNotification_invite(remoteMessage);
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    @Override
    public void onMessageSent(@NonNull String s) {
        super.onMessageSent(s);
    }

    @Override
    public void onSendError(@NonNull String s, @NonNull Exception e) {
        super.onSendError(s, e);
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        Log.d(TAG, "Refreshed token: " + s);
        MyApplication.FCMtoken = s;
        Call call = RetrofitClient.getInstance().getApi().registerFirebaseToken(MyApplication.token,s,MyApplication.deviceId,1,"1.0");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }
            @Override
            public void onFailure(Call call, Throwable t) {

            }
        });
        SharedPreferences sharedPreferences = getSharedPreferences("token",MODE_PRIVATE);
        Editor editor = sharedPreferences.edit();
        editor.putString("FCMtoken",s);
        editor.apply();
    }
    private void sendNotification_invite(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();
        Intent viewInfoIntent = new Intent(this, ManageTourHistory.class);
        int tourId = Integer.parseInt(data.get("id"));
        viewInfoIntent.putExtra("tourId",tourId);
        viewInfoIntent.putExtra("history",0);
        viewInfoIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent_view = PendingIntent.getActivity(this, 0, viewInfoIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Intent intentYes = new Intent();
        intentYes.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intentYes.setAction(MyApplication.YES_ACTION);
        intentYes.putExtra("tourId",data.get("id"));
        PendingIntent pendingIntentYes = PendingIntent.getBroadcast(this, 0, intentYes, PendingIntent.FLAG_UPDATE_CURRENT);
        Intent intentNo = new Intent();
        intentNo.putExtra("tourId",data.get("id"));
        intentNo.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intentNo.setAction(MyApplication.NO_ACTION);
        PendingIntent pendingIntentNo = PendingIntent.getBroadcast(this, 0, intentNo, PendingIntent.FLAG_UPDATE_CURRENT);

        final String channelId = getString(R.string.project_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_icons)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_icons))
                        .setContentTitle(getString(R.string.project_id))
                        .setContentText("Bạn đã nhận được lời mời tham gia tour "+data.get("name"))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent_view)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setPriority(NotificationManager.IMPORTANCE_HIGH);
        notificationBuilder.addAction(R.drawable.ic_check,getString(R.string.accept_invitation),pendingIntentYes);
        notificationBuilder.addAction(R.drawable.ic_clear,getString(R.string.deny_invitation),pendingIntentNo);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
        }
        ReceiveNotiButtonAction receiveNotiButtonAction = new ReceiveNotiButtonAction();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MyApplication.YES_ACTION);
        intentFilter.addAction(MyApplication.NO_ACTION);
        notificationManager.notify(tourId, notificationBuilder.build());
    }
}
