package com.ygaps.travelapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotiOnRoad {
  @SerializedName("lat")
  @Expose
  private String latitude;
  @SerializedName("long")
  @Expose
  private String longtitude;
  @SerializedName("note")
  @Expose
  private String note;
  @SerializedName("notificationType")
  @Expose
  private int notiType;
  @SerializedName("speed")
  @Expose
  private Integer speed;

  public Integer getSpeed() {
    return speed;
  }

  public void setSpeed(Integer speed) {
    this.speed = speed;
  }

  public String getLatitude() {
    return latitude;
  }

  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }

  public String getLongtitude() {
    return longtitude;
  }

  public void setLongtitude(String longtitude) {
    this.longtitude = longtitude;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public int getNotiType() {
    return notiType;
  }

  public void setNotiType(int notiType) {
    this.notiType = notiType;
  }
}
