package com.ygaps.travelapp.data.model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StopPoint {
    @Nullable
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lat")
    @Expose
    private String latitude;
    @SerializedName("long")
    @Expose
    private String longtitude;
    @SerializedName("arrivalAt")
    @Expose
    private String arrivalTime;
    @SerializedName("leaveAt")
    @Expose
    private String leaveTime;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("minCost")
    @Expose
    private String minCost;
    @SerializedName("maxCost")
    @Expose
    private String maxCost;
    @SerializedName("serviceTypeId")
    @Expose
    private int serviceTypeId;
    @Nullable
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("provinceId")
    @Expose
    private int provinceId;

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getLeaveTime() {
        return leaveTime;
    }

    public void setLeaveTime(String leaveTime) {
        this.leaveTime = leaveTime;
    }

    public String getMinCost() {
        return minCost;
    }

    public void setMinCost(String minCost) {
        this.minCost = minCost;
    }

    public String getMaxCost() {
        return maxCost;
    }

    public void setMaxCost(String maxCost) {
        this.maxCost = maxCost;
    }

    public int getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(int serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public StopPoint(int id, String name, String latitude, String longtitude, String arrivalTime, String leaveTime, String address, String minCost, String maxCost, int serviceTypeId, String avatar, int provinceId) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longtitude = longtitude;
        this.arrivalTime = arrivalTime;
        this.leaveTime = leaveTime;
        this.address = address;
        this.minCost = minCost;
        this.maxCost = maxCost;
        this.serviceTypeId = serviceTypeId;
        this.avatar = avatar;
        this.provinceId = provinceId;
    }

    public StopPoint(String name, String latitude, String longtitude, String arrivalTime, String leaveTime, String address, String minCost, String maxCost, int serviceTypeId, String avatar, int provinceId) {
        this.name = name;
        this.latitude = latitude;
        this.longtitude = longtitude;
        this.arrivalTime = arrivalTime;
        this.leaveTime = leaveTime;
        this.address = address;
        this.minCost = minCost;
        this.maxCost = maxCost;
        this.serviceTypeId = serviceTypeId;
        this.avatar = avatar;
        this.provinceId = provinceId;
    }

}
