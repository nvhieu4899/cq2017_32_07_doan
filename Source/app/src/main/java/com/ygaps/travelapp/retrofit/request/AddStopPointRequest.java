package com.ygaps.travelapp.retrofit.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.data.model.StopPoint;

import java.io.Serializable;
import java.util.ArrayList;

public class AddStopPointRequest implements Serializable{

    @SerializedName("tourId")
    @Expose
    private Integer tourId;
    @SerializedName("stopPoints")
    @Expose
    private ArrayList<StopPoint> stopPoints = null;
    @SerializedName("deleteIds")
    @Expose
    private ArrayList<Integer> deleteIds = null;
    public Integer getTourId() {
        return tourId;
    }

    public void setTourId(Integer tourId) {
        this.tourId = tourId;
    }

    public ArrayList<StopPoint> getStopPoints() {
        return stopPoints;
    }

    public void setStopPoints(ArrayList<StopPoint> stopPoints) {
        this.stopPoints = stopPoints;
    }

    public ArrayList<Integer> getDeleteIds() {
        return deleteIds;
    }

    public void setDeleteIds(ArrayList<Integer> deleteIds) {
        this.deleteIds = deleteIds;
    }

    public AddStopPointRequest(Integer tourId, ArrayList<StopPoint> stopPoints, ArrayList<Integer> deleteIds) {
        this.tourId = tourId;
        this.stopPoints = stopPoints;
        this.deleteIds = deleteIds;
    }
}
