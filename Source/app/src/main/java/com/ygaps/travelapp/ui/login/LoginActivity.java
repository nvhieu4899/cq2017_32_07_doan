package com.ygaps.travelapp.ui.login;

import androidx.annotation.NonNull;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.ygaps.travelapp.Forgot;
import com.ygaps.travelapp.Main;
import com.ygaps.travelapp.MyApplication;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.SignUp;
import com.ygaps.travelapp.retrofit.RetrofitClient;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private TextInputLayout lg_username;
    private TextInputLayout lg_password;
    private TextView lg_forgotpassword;
    private Button lg;
    private TextView lg_sign;
    int RC_SIGN_IN = 9001;
    private static final String TAG = "SignInActivity";
    private GoogleApiClient mGoogleApiClient;
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        AccessToken.setCurrentAccessToken(null);
        loginButton = (LoginButton) findViewById(R.id.login_facebook);
        lg_username = (TextInputLayout)findViewById(R.id.login_username);
        lg_password = (TextInputLayout)findViewById(R.id.login_password);
        lg_forgotpassword = (TextView) findViewById(R.id.login_forgotpassword);
        lg = (Button) findViewById((R.id.login));
        lg_sign = (TextView) findViewById(R.id.login_sign);
        callbackManager = CallbackManager.Factory.create();
        SignInButton signInButton = findViewById(R.id.login_google);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        String serverClientId = getString(R.string.server_client_id);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.DRIVE_APPFOLDER))
                .requestServerAuthCode(serverClientId)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        lg_forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Forgot.class);
                startActivity(intent);
            }
        });

        lg_sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SignUp.class);
                startActivity(intent);
            }
        });

        lg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lg.setEnabled(false);
                if (validateUser()&validatePassword()){
                    final String user = lg_username.getEditText().getText().toString().trim();
                    final String password = lg_password.getEditText().getText().toString().trim();
                    retrofit2.Call call = RetrofitClient
                            .getInstance()
                            .getApi()
                            .loginUser(user,password);

                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                           String res=null;
                            try {
                                if (response.code() == 200) {
                                    Toast.makeText(getApplicationContext(), R.string.success_login, Toast.LENGTH_LONG).show();
                                    User us = new User(0,null);
                                    String s = response.body().string();
                                    try {
                                        JSONObject jsonObject = new JSONObject(s);
                                        us.setUserId(jsonObject.getInt("userId"));
                                        us.setToken(jsonObject.getString("token"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    Intent intent = new Intent(getApplicationContext(), Main.class);
                                    intent.putExtra("token", us.getToken());
                                    intent.putExtra("id", us.getUserId());
                                    SharedPreferences sharedPreferences = getSharedPreferences("token",Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString("token",us.getToken());
                                    editor.apply();
                                    MyApplication.token=us.getToken();
                                    MyApplication.userId = String.valueOf(us.getUserId());
                                    startActivity(intent);
                                    finish();
                                } else {
                                    res = response.errorBody().string();
                                }
                            } catch (IOException e)
                            {
                                e.printStackTrace();
                            }
                            if (res!=null)
                            {
                                try{
                                    JSONObject jsonObject = new JSONObject(res);
                                    Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    });
                }
                lg.setEnabled(true);
            }
        });

        set_Login();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            String s= null;
            if(result.isSuccess()) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                try {
                    GoogleSignInAccount account = task.getResult(ApiException.class);
                    s = account.getServerAuthCode().toString();
                    retrofit2.Call call = RetrofitClient
                            .getInstance()
                            .getApi()
                            .logingg(s);

                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            String res = null;
                            try {
                                if (response.code() == 200) {
                                    Toast.makeText(getApplicationContext(), R.string.success_login, Toast.LENGTH_LONG).show();
                                    String t1 = null, t2 = null, t3 = null;
                                    String re = response.body().string();
                                    try {
                                        JSONObject jsonObject = new JSONObject(re);
                                        t3 = jsonObject.getString("avatar");
                                        t2 = jsonObject.getString("fullName");
                                        t1 = jsonObject.getString("token");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    Toast.makeText(getApplicationContext(), t1, Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(getApplicationContext(), Main.class);
                                    intent.putExtra("token", t1);
                                    intent.putExtra("fullName", t2);
                                    intent.putExtra("avatar", t3);
                                    SharedPreferences sharedPreferences = getSharedPreferences("token",Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString("token",t1);
                                    editor.apply();
                                    startActivity(intent);
                                    finish();
                                } else {
                                    res = response.errorBody().string();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if (res != null) {
                                try {
                                    JSONObject jsonObject = new JSONObject(res);
                                    Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (ApiException e) {
                    e.printStackTrace();
                }
            }else{

                Toast.makeText(getApplicationContext(),"Login failed!",Toast.LENGTH_LONG).show();
            }
        }
    }

    private void set_Login(){
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String s = AccessToken.getCurrentAccessToken().getToken();
                retrofit2.Call call = RetrofitClient
                        .getInstance()
                        .getApi()
                        .loginfb(s);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        String res=null;
                        try {
                            if (response.code() == 200) {
                                Toast.makeText(getApplicationContext(), R.string.success_login, Toast.LENGTH_LONG).show();
                                User us = new User(0,null);
                                String s = response.body().string();
                                try {
                                    JSONObject jsonObject = new JSONObject(s);
                                    us.setUserId(jsonObject.getInt("userId"));
                                    us.setToken(jsonObject.getString("token"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Intent intent = new Intent(getApplicationContext(), Main.class);
                                intent.putExtra("token", us.getToken());
                                intent.putExtra("id", us.getUserId());
                                SharedPreferences sharedPreferences = getSharedPreferences("token",Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("token",us.getToken());
                                editor.apply();
                                MyApplication.token=us.getToken();
                                MyApplication.userId = String.valueOf(us.getUserId());
                                startActivity(intent);
                                finish();
                            } else {
                                res = response.errorBody().string();
                            }
                        } catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                        if (res!=null)
                        {
                            try{
                                JSONObject jsonObject = new JSONObject(res);
                                Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }


    private boolean validateUser()
    {
        String input = lg_username.getEditText().getText().toString().trim();
        if(input.isEmpty())
        {
            lg_username.setError(getString(R.string.str_required));
            return false;
        } else {
            lg_username.setError(null);
        }

        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(input).matches() && !android.util.Patterns.PHONE.matcher(input).matches())
        {
            lg_username.setError(getString(R.string.str_invalidusername));
            return false;
        } else {
            lg_username.setError(null);
        }
        return true;
    }

    private boolean validatePassword()
    {
        String input = lg_password.getEditText().getText().toString().trim();
        if(input.isEmpty())
        {
            lg_password.setError(getString(R.string.str_required));
            return false;
        } else {
            lg_password.setError(null);
        }
        return true;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("Failed",connectionResult + "");
    }
}
