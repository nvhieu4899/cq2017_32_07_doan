package com.ygaps.travelapp.TourActivities.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.ygaps.travelapp.MovingActivity;
import com.ygaps.travelapp.MyApplication;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.TourActivities.AddStopPointActivity;
import com.ygaps.travelapp.data.model.TourInfo;
import com.ygaps.travelapp.retrofit.RetrofitClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A placeholder fragment containing a simple view.
 */
public class TourInfoFragment extends Fragment {

  private static final String ARG_SECTION_NUMBER = "section_number";
  
  private PageViewModel pageViewModel;

  public static TourInfoFragment newInstance() {
    TourInfoFragment fragment = new TourInfoFragment();
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    pageViewModel = ViewModelProviders.of(getActivity()).get(PageViewModel.class);
  }

  @Override
  public View onCreateView(
          @NonNull final LayoutInflater inflater, ViewGroup container,
          Bundle savedInstanceState) {
    View root = inflater.inflate(R.layout.fragment_tour_info, container, false);

    final TextView startDate = root.findViewById(R.id.updatetour_startdate);
    final TextView endDate = root.findViewById(R.id.updatetour_enddate);
    final CheckBox isPrivated = root.findViewById(R.id.isPrivated);
    final TextView adultView = root.findViewById(R.id.adults);
    final TextView childView = root.findViewById(R.id.children);
    final TextView name = root.findViewById(R.id.name);
    final Button viewStopPoints = root.findViewById(R.id.map_pick);
    final Button statusbt = root.findViewById(R.id.statusbt);
    final Spinner spinner = root.findViewById(R.id.spinnerChose);
    pageViewModel.getObserveable().observe(getViewLifecycleOwner(), new Observer<TourInfo>() {
      @Override
      public void onChanged(final TourInfo tourInfo) {
        if (tourInfo!=null) {
          int status = tourInfo.getStatus();
          if(status != 1 && MyApplication.userId.equals(tourInfo.getHostId())) {
            statusbt.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                String idoftour = String.format("%s", tourInfo.getId());
                retrofit2.Call call;
                call = RetrofitClient
                        .getInstance()
                        .getApi()
                        .UpdateTour(MyApplication.token, idoftour, null, null, null, null, null, null, null, null, null, null, null, null, null, 1);

                call.enqueue(new Callback<ResponseBody>() {
                  @Override
                  public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    String res = null;
                    try {
                      if (response.code() == 200) {
                        Intent intent = new Intent(getApplicationContext(), MovingActivity.class);
                        intent.putExtra("tourId", tourInfo.getId());
                        startActivity(intent);
                      } else {
                        res = response.errorBody().string();
                      }
                    } catch (IOException e) {
                      e.printStackTrace();
                    }
                    if (res != null) {
                      try {
                        JSONObject jsonObject = new JSONObject(res);
                        Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                      } catch (JSONException e) {
                        e.printStackTrace();
                      }
                    }
                  }

                  @Override
                  public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                  }
                });
              }
            });
          }else statusbt.setVisibility(View.GONE);

          isPrivated.setChecked(tourInfo.getisPrivate());
          name.setText(tourInfo.getName());
          childView.setText(String.valueOf(tourInfo.getChilds()));
          adultView.setText(String.valueOf(tourInfo.getAdults()));
          SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
          if (tourInfo.getStartDate()!=null) {
            Date start = new Date(Long.valueOf(tourInfo.getStartDate()));
            startDate.setText(simpleDateFormat.format(start));
          }
          if (tourInfo.getEndDate()!=null)
          {
            {
              Date end = new Date(Long.valueOf(tourInfo.getEndDate()));
              endDate.setText(simpleDateFormat.format(end));
            }}
          viewStopPoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent intent = new Intent(getContext(), AddStopPointActivity.class);
              intent.putExtra("tourId",tourInfo.getId());
              intent.putExtra("isEditable",false);
              startActivity(intent);
            }
          });
        }
      }
    });
    return root;
  }
}