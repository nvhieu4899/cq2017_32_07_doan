package com.ygaps.travelapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NotiList {
  @SerializedName("notiList")
  @Expose
  private ArrayList<NotiOnRoad> notiOnRoads;

  public ArrayList<NotiOnRoad> getNotiOnRoads() {
    return notiOnRoads;
  }

  public void setNotiOnRoads(ArrayList<NotiOnRoad> notiOnRoads) {
    this.notiOnRoads = notiOnRoads;
  }
}
