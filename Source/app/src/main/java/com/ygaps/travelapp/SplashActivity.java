package com.ygaps.travelapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.widget.Toast;

import com.ygaps.travelapp.retrofit.RetrofitClient;
import com.ygaps.travelapp.ui.login.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    String token = MyApplication.token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (token==null||token.isEmpty())
        {
            startActivity(new Intent(getApplicationContext(),LoginActivity.class));
            finish();
        }
        else
        {
            Call call = RetrofitClient.getInstance().getApi().getUserInfo(token);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful())
                    {
                        try
                        {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            String fullName = jsonObject.getString("fullName");
                            int userId = jsonObject.getInt("id");
                            SharedPreferences sharedPreferences = getSharedPreferences("token",MODE_PRIVATE);
                            Editor editor = sharedPreferences.edit();
                            editor.putString("userId",String.valueOf(userId));


                            MyApplication.userId = String.valueOf(userId);
                            editor.commit();


                            Intent intent = new Intent(getApplicationContext(), Main.class);
                            intent.putExtra("token",token);
                            intent.putExtra("name",fullName);
                            intent.putExtra("userId",userId);
                            MyApplication.userId= String.valueOf(userId);
                            startActivityForResult(intent,100);
                        }
                        catch (JSONException | IOException e)
                        {
                            e.printStackTrace();
                        }

                    }
                    else
                    {
                        if (response.code()==401)
                        {
                            Toast.makeText(getApplicationContext(),getString(R.string.token_auth_failed),Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                            finish();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),getString(R.string.getUserInfo_503),Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                            finish();

                        }
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                    finish();
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) finish();
    }
}
