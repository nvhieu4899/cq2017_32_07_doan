package com.ygaps.travelapp.TourActivities;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ygaps.travelapp.R;

import java.util.ArrayList;


public class PickSourceAndDestination extends FragmentActivity implements OnMapReadyCallback {
    private Marker endMark, startMark;
    private TextView instruction;
    private GoogleMap mMap;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        instruction = (TextView)findViewById(R.id.instructor_textview_map);
        button = (Button) findViewById(R.id.pick_or_view_Done_btn);
        button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent();
                        intent.putExtra("srcLat", startMark.getPosition().latitude);
                        intent.putExtra("srcLng", startMark.getPosition().longitude);
                        intent.putExtra("desLat", endMark.getPosition().latitude);
                        intent.putExtra("desLng", endMark.getPosition().longitude);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }

                }
        );

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }
    public static BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap((int)(vectorDrawable.getIntrinsicWidth()), (int)(vectorDrawable.getIntrinsicHeight()), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
            mMap = googleMap;
            Intent intent = getIntent();
            double startLat = intent.getDoubleExtra("startPointLat", 10.762);
            double startLng = intent.getDoubleExtra("startPointLong", 106.660172);
            double endLat = intent.getDoubleExtra("endPointLat", 21.028511);
            double endLng = intent.getDoubleExtra("endPointLong", 105.804817);
            ArrayList<LatLng> stopPoint = new ArrayList<LatLng>();
            stopPoint.add(new LatLng(startLat, startLng));
            stopPoint.add(new LatLng(endLat, endLng));

            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (LatLng latLng : stopPoint) {
                builder.include(latLng);
            }
            LatLngBounds latLngBounds = builder.build();
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (width * 0.18);
            startMark = mMap.addMarker(new MarkerOptions()
                    .position(stopPoint.get(0))
                    .title(getString(R.string.marker_startP))
                    .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_startpointmarker)).draggable(true));
            endMark = mMap.addMarker(new MarkerOptions()
                    .position(stopPoint.get(1))
                    .title(getString(R.string.marker_endP))
                    .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_endpointmarker)).draggable(true));
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, width, height, padding));


            mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                @Override
                public void onMarkerDragStart(Marker marker) {
                    if (marker.equals(startMark)) {
                        instruction.setText(getString(R.string.str_dragmarker_start));
                    }
                    if (marker.equals(endMark)) {
                        instruction.setText(getString(R.string.str_dragmarker_end));
                    }
                }
                @Override
                public void onMarkerDrag(Marker marker) {

                }

                @Override
                public void onMarkerDragEnd(Marker marker) {
                    instruction.setText("");
                }
            });
        }
    @Override
    public void onBackPressed(){
        setResult(Activity.RESULT_CANCELED);
        super.onBackPressed();
    }

}


