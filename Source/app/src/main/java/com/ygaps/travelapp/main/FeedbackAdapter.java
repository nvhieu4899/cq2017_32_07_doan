package com.ygaps.travelapp.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ygaps.travelapp.R;
import com.ygaps.travelapp.data.model.Feedback;
import com.ygaps.travelapp.data.model.Reviewer;
import com.ygaps.travelapp.retrofit.APIUtils;
import com.ygaps.travelapp.retrofit.DataClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolder> {
    List<Feedback> feedbacks;
    Context context;

    public FeedbackAdapter(List<Feedback> feedbacks, Context context) {
        this.feedbacks = feedbacks;
        this.context = context;
    }

    @NonNull
    @Override
    public FeedbackAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView = layoutInflater.inflate(R.layout.recycler_item_feedback, parent, false);

        return new FeedbackAdapter.ViewHolder(itemView);
    }

    public static Drawable LoadImageFromWebOperations(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }

    public static String FormatDate(String milisecondString) {
        if (milisecondString==null) return "chưa biết";

        long milisec = Long.parseLong(milisecondString);
        Date date = new Date(milisec);
        @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("dd - MM - yyyy");
        return dateFormat.format(date);
    }

    @Override
    public void onBindViewHolder(@NonNull final FeedbackAdapter.ViewHolder holder, final int position) {

        if(feedbacks.get(position).getAvatar() == null) {
            holder.imgReviewer.setImageResource(R.drawable.img_default);
        }
        else {
            holder.imgReviewer.setImageDrawable(LoadImageFromWebOperations(feedbacks.get(position).getAvatar()));
        }

        holder.nameReviewer.setText(feedbacks.get(position).getName());

        holder.content.setText(feedbacks.get(position).getFeedback());

        holder.pointRating.setRating(feedbacks.get(position).getPoint());
        holder.pointRating.setEnabled(false);

        holder.report_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popup;
                popup = new PopupMenu(context, holder.report_btn);
                popup.inflate(R.menu.option_report);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu1:

                                SharedPreferences sharedPreferences = Objects.requireNonNull(context).getSharedPreferences("token", Context.MODE_PRIVATE);
                                String token = sharedPreferences.getString("token", "");

                                DataClient dataClient = APIUtils.getData();
                                Call<ResponseBody> callbackReportReview = dataClient.postReportFeedback(token, feedbacks.get(position).getUserId());
                                callbackReportReview.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        String res=null;
                                        try {
                                            if (response.code() == 200) {


                                                String s = response.body().string();
                                                try {
                                                    JSONObject jsonObject = new JSONObject(s);
                                                    Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            } else {
                                                res = response.errorBody().string();
                                            }
                                        } catch (IOException e)
                                        {
                                            e.printStackTrace();
                                        }
                                        if (res!=null)
                                        {
                                            try{
                                                JSONObject jsonObject = new JSONObject(res);
                                                Toast.makeText(context,jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                                    }
                                });

                                break;

                        }
                        return false;
                    }
                });

                popup.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return feedbacks.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircleImageView imgReviewer;
        TextView nameReviewer;
        TextView date;
        RatingBar pointRating;
        TextView content;
        ImageButton report_btn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgReviewer =  itemView.findViewById(R.id.profile_image);
            nameReviewer = itemView.findViewById(R.id.name_text);
            date = itemView.findViewById(R.id.date_review);
            pointRating = itemView.findViewById(R.id.ratingBar);
            content = itemView.findViewById(R.id.content_review);
            report_btn = itemView.findViewById(R.id.report_btn);

            itemView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {
            // do something
        }
    }
}
