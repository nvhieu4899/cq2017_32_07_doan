package com.ygaps.travelapp.TourActivities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.ygaps.travelapp.MovingActivity;
import com.ygaps.travelapp.MyApplication;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.TourActivities.ui.main.PageViewModel;
import com.ygaps.travelapp.TourActivities.ui.main.SectionsPagerAdapter;
import com.ygaps.travelapp.data.model.TourInfo;
import com.ygaps.travelapp.retrofit.RetrofitClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageTourHistory extends AppCompatActivity {
  private int tourId;
  private int history;
  private String hostId;
  private int status;
  private TourInfo tourInfo;
  private PageViewModel pageViewModel;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_manage_tour2);

    tourId = getIntent().getIntExtra("tourId", -1);
    history = getIntent().getIntExtra("history", -1);
    hostId = getIntent().getStringExtra("hostid");
    status = getIntent().getIntExtra("status", -2);
    SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager(), history);
    ViewPager viewPager = findViewById(R.id.view_pager);
    viewPager.setAdapter(sectionsPagerAdapter);
    TabLayout tabs = findViewById(R.id.tabs);
    tabs.setupWithViewPager(viewPager);
    FloatingActionButton fab = findViewById(R.id.fab);

    pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
    pageViewModel.setHistory(history);
    Call call = RetrofitClient.getInstance().getApi().getTourInfo(MyApplication.token, tourId);
    call.enqueue(new Callback<TourInfo>() {
      @Override
      public void onResponse(Call<TourInfo> call, Response<TourInfo> response) {
        if (response.isSuccessful()) {
          pageViewModel.setTour(response.body());
        }
      }

      @Override
      public void onFailure(Call call, Throwable t) {

      }
    });

    if (history == 1) {
      fab.show();
      if (status==1) {
        fab.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            Intent intent = new Intent(getApplicationContext(), MovingActivity.class);
            intent.putExtra("tourId", tourId);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
          }
        });
      } else fab.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          Toast.makeText(getApplicationContext(), "Liên hệ host để bắt đầu", Toast.LENGTH_LONG).show();
        }
      });
    } else
    {
      fab.hide();
    }
    final Spinner spinner = (Spinner) findViewById(R.id.spinnerChose);
    if (hostId!=null && hostId.equals(MyApplication.userId)) {
      ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.update_tour_spinner, android.R.layout.simple_spinner_item);
      adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
      spinner.setAdapter(adapter);
      spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
          if (position == 1) {
            Intent intent = new Intent(getApplicationContext(), Updatetour.class);
            intent.putExtra("tourId", tourId);
            startActivityForResult(intent, position);
          }
          if (position == 2) {
            Intent intent = new Intent(getApplicationContext(), AddStopPointActivity.class);
            intent.putExtra("tourId", tourId);
            intent.putExtra("isEditable",true);
            startActivityForResult(intent, position);
          }
          if (position == 3)
          {
            AlertDialog.Builder alert = new AlertDialog.Builder(ManageTourHistory.this);
            alert.setTitle(R.string.delete);
            alert.setMessage(R.string.delete_quest);
            alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

              public void onClick(DialogInterface dialog, int which) {
                String id = String.format("%s", tourId);
                retrofit2.Call call;
                call = RetrofitClient
                        .getInstance()
                        .getApi()
                        .UpdateTour(MyApplication.token, id, null, null, null, null, null, null, null, null, null, null, null, null, null, -1);

                call.enqueue(new Callback<ResponseBody>() {
                  @Override
                  public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    String res = null;
                    try {
                      if (response.code() == 200) {
                        Toast.makeText(getApplicationContext(), R.string.delete_tour_sc, Toast.LENGTH_LONG).show();
                        finish();
                      } else {
                        res = response.errorBody().string();
                      }
                    } catch (IOException e) {
                      e.printStackTrace();
                    }
                    if (res != null) {
                      try {
                        JSONObject jsonObject = new JSONObject(res);
                        Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                      } catch (JSONException e) {
                        e.printStackTrace();
                      }
                    }
                  }

                  @Override
                  public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                  }
                });
              }
            });
            alert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int which) {
                // close dialog
                dialog.cancel();
                Intent refresh = new Intent(ManageTourHistory.this, ManageTourHistory.class);
                refresh.putExtra("tourId", tourId);
                refresh.putExtra("history", 1);
                refresh.putExtra("hostid", hostId);
                refresh.putExtra("status", status);
                startActivity(refresh);
                ManageTourHistory.this.finish();
              }
            });
            alert.show();
          }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
      });
    }
    else spinner.setVisibility(View.GONE);
  }

  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode==1 || requestCode==2)
    {
      Intent refresh = new Intent(this, ManageTourHistory.class);
      refresh.putExtra("tourId", tourId);
      refresh.putExtra("history", 1);
      refresh.putExtra("hostid", hostId);
      refresh.putExtra("status", status);
      startActivity(refresh);
      this.finish();
    }
  }
}