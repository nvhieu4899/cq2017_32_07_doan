package com.ygaps.travelapp.Nav_Fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.ygaps.travelapp.MyApplication;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.data.model.Tour;
import com.ygaps.travelapp.data.model.Tours;
import com.ygaps.travelapp.main.HistoryAdapter;
import com.ygaps.travelapp.main.ToursAdapter;
import com.ygaps.travelapp.retrofit.APIUtils;
import com.ygaps.travelapp.retrofit.DataClient;
import com.ygaps.travelapp.retrofit.RetrofitClient;
import com.ygaps.travelapp.ui.login.LoginActivity;
import com.ygaps.travelapp.ui.login.User;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class HistoryFragment extends Fragment {

  private ArrayList<Tour> tours = new ArrayList<>();
  private RecyclerView recyclerView;
  private int loadedPage = 1;
  private final int PAGE_SIZE = 10;
  private int totalsize = 0;
  private HistoryAdapter historyAdapter;
  private boolean isLoading = false;
  private ImageButton refresh;
  public HistoryFragment() {
    // Required empty public constructor
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_history, container, false);
    recyclerView = (RecyclerView) view.findViewById(R.id.recyclerTours);
    refresh = view.findViewById(R.id.refresh);
    Intent intent = getActivity().getIntent();
    Bundle extras = intent.getExtras();
    if (extras != null) {
      getListTourThenInitRecyclerView();
      refresh.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          DataClient dataClient = APIUtils.getData();
          retrofit2.Call<Tours> callback = dataClient.getHistoryTourOfUser(MyApplication.token, 1, String.valueOf(PAGE_SIZE));
          callback.enqueue(new Callback<Tours>() {
            @Override
            public void onResponse(Call<Tours> call, Response<Tours> response) {
              if (response.isSuccessful()) {
                tours.clear();
                tours.addAll(response.body().getTours());
                totalsize = Integer.parseInt(response.body().getTotal());
                historyAdapter.notifyDataSetChanged();
                loadedPage = 1;
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                linearLayoutManager.scrollToPositionWithOffset(0,0);
              }
            }
            @Override
            public void onFailure(Call<Tours> call, Throwable t) {
            }
          });
        }
      });
    } else {
      // trở lại màn hình login
      Intent intentLogin = new Intent(getApplicationContext(), LoginActivity.class);
      startActivity(intent);
    }
    return view;
  }

  private void getListTourThenInitRecyclerView() {
    recyclerView.setHasFixedSize(true);
    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
    recyclerView.setLayoutManager(layoutManager);
    historyAdapter = new HistoryAdapter(tours, getApplicationContext());
    recyclerView.setAdapter(historyAdapter);
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
      }
      @Override
      public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (!isLoading) {
          if (linearLayoutManager != null && linearLayoutManager.findLastVisibleItemPosition() == tours.size() - 1) {
            loadMoreTour();
            isLoading = true;
          }
        }
      }
    });
    DataClient dataClient = APIUtils.getData();
    retrofit2.Call<Tours> callback = dataClient.getHistoryTourOfUser(MyApplication.token, 1, String.valueOf(PAGE_SIZE));
    callback.enqueue(new Callback<Tours>() {
      @Override
      public void onResponse(Call<Tours> call, Response<Tours> response) {
        if (response.isSuccessful()) {
          tours.clear();
          tours.addAll(response.body().getTours());
          totalsize = Integer.parseInt(response.body().getTotal());
          historyAdapter.notifyDataSetChanged();
        }
      }
      @Override
      public void onFailure(Call<Tours> call, Throwable t) {

      }
    });
  }

  private void loadMoreTour() {
    Handler handler = new Handler();
    handler.postDelayed(new Runnable() {
      @Override
      public void run() {
        if (tours.size() != totalsize) {
          Call call = RetrofitClient.getInstance().getApi().getHistoryTourOfUser(MyApplication.token,
                  loadedPage + 1,
                  String.valueOf(PAGE_SIZE));
          call.enqueue(new Callback<Tours>() {
            @Override
            public void onResponse(Call<Tours> call, Response<Tours> response) {
              if (response.isSuccessful()) {
                tours.addAll(response.body().getTours());
                historyAdapter.notifyDataSetChanged();
                totalsize = Integer.parseInt(response.body().getTotal());
                loadedPage++;
              }
              isLoading = false;
            }

            @Override
            public void onFailure(Call<Tours> call, Throwable t) {
              isLoading = false;
            }
          });
        }
      }
    }, 500);
  }
}
