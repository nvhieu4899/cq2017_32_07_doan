package com.ygaps.travelapp.Nav_Fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ygaps.travelapp.MyApplication;
import com.ygaps.travelapp.TourActivities.CreateTour;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.data.model.Tour;
import com.ygaps.travelapp.data.model.Tours;
import com.ygaps.travelapp.main.ToursAdapter;
import com.ygaps.travelapp.retrofit.APIUtils;
import com.ygaps.travelapp.retrofit.DataClient;
import com.ygaps.travelapp.retrofit.RetrofitClient;
import com.ygaps.travelapp.ui.login.LoginActivity;
import com.ygaps.travelapp.ui.login.User;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListFragment extends Fragment {

  private ArrayList<Tour> tours = new ArrayList<>();

  private ImageView addbt;
  RecyclerView recyclerView;

  ToursAdapter toursAdapter;
  int total;

  boolean isLoading = false;
  int loadedPage;
  final int PAGE_SIZE = 10;

  public ListFragment() {
    // Required empty public constructor
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_list, container, false);

    loadedPage = 1;

    addbt = (ImageView) view.findViewById(R.id.add_bt);
    recyclerView = (RecyclerView) view.findViewById(R.id.recyclerTours);
    initRecyclerView(tours);


    addbt.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(), CreateTour.class);
        startActivity(intent);
      }
    });

    Intent intent = getActivity().getIntent();
    Bundle extras = intent.getExtras();


    if (extras != null) {
      getListTourThenInitRecyclerView();
      recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
          super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
          super.onScrolled(recyclerView, dx, dy);

          LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
          if (!isLoading) {
            if (linearLayoutManager != null && linearLayoutManager.findLastVisibleItemPosition() == tours.size() - 1) {
              loadMoreTour();
              isLoading = true;
            }
          }
        }
      });


      // and get whatever type user account id is
    } else {
      // trở lại màn hình login
      Intent intentLogin = new Intent(getApplicationContext(), LoginActivity.class);
      startActivity(intent);
    }


    return view;
  }

  public void initRecyclerView(ArrayList<Tour> tours) {
    recyclerView.setHasFixedSize(true);
    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
    recyclerView.setLayoutManager(layoutManager);
    toursAdapter = new ToursAdapter(tours, getApplicationContext());
    recyclerView.setAdapter(toursAdapter);
  }

  public void getListTourThenInitRecyclerView() {
    DataClient dataClient = APIUtils.getData();
    retrofit2.Call<Tours> callback = dataClient.getListTour(MyApplication.token, PAGE_SIZE, 1, "name", false);
    callback.enqueue(new Callback<Tours>() {
      @Override
      public void onResponse(Call<Tours> call, Response<Tours> response) {
        if (response.isSuccessful()) {
          tours.clear();
          tours.addAll(response.body().getTours());
          total = Integer.parseInt(response.body().getTotal());
          toursAdapter.notifyDataSetChanged();
        }
      }

      @Override
      public void onFailure(Call<Tours> call, Throwable t) {

      }
    });
  }

  private void loadMoreTour() {
    Handler handler = new Handler();
    handler.postDelayed(new Runnable() {
      @Override
      public void run() {
        if (tours.size() != total) {
          Call call = RetrofitClient
                  .getInstance()
                  .getApi()
                  .getListTour(MyApplication.token
                          , PAGE_SIZE
                          , loadedPage+1
                          , "name"
                          , false);
          call.enqueue(new Callback<Tours>() {
            @Override
            public void onResponse(Call<Tours> call, Response<Tours> response) {
              if (response.isSuccessful()) {
                tours.addAll(response.body().getTours());
                toursAdapter.notifyDataSetChanged();
                total = Integer.parseInt(response.body().getTotal());
                loadedPage++;
              }
              isLoading = false;
            }

            @Override
            public void onFailure(Call<Tours> call, Throwable t) {
              isLoading = false;
            }
          });
        }
      }
    }, 2000);
  }

}
