package com.ygaps.travelapp.retrofit;

import com.ygaps.travelapp.data.model.Feedbacks;
import com.ygaps.travelapp.data.model.ListInvitation;
import com.ygaps.travelapp.data.model.MemberCoordinate;
import com.ygaps.travelapp.data.model.MessageResponseRetrofit;
import com.ygaps.travelapp.data.model.NotiList;
import com.ygaps.travelapp.data.model.PointStats;
import com.ygaps.travelapp.data.model.Reviewers;
import com.ygaps.travelapp.data.model.StopPoint;
import com.ygaps.travelapp.data.model.StopPoints;
import com.ygaps.travelapp.data.model.UserSearchResult;
import com.ygaps.travelapp.retrofit.request.AddStopPointRequest;
import com.ygaps.travelapp.data.model.Notifications;
import com.ygaps.travelapp.data.model.TourInfo;
import com.ygaps.travelapp.data.model.Tours;
import com.ygaps.travelapp.retrofit.request.GetOneSuggestSP;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface DataClient {

    @FormUrlEncoded
    @POST("user/register")
    Call<ResponseBody> createUser(
            @Field("password") String password,
            @Field("fullName") String name,
            @Field("email") String email,
            @Field("phone") String phonenumber,
            @Field("address") String address,
            @Field("dob") String dob,
            @Field("gender") int gender
    );
    @FormUrlEncoded
    @POST("user/edit-info")
    Call<ResponseBody> editUser(
            @Header("Authorization") String token,
            @Field("fullName") String name,
            @Field("email") String email,
            @Field("phone") String phonenumber,
            @Field("dob") String dob,
            @Field("gender") int gender
    );

    @FormUrlEncoded
    @POST("user/login")
    Call<ResponseBody> loginUser(
            @Field("emailPhone") String emailPhone,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("user/request-otp-recovery")
    Call<ResponseBody> forgotUser(
            @Field("type") String type,
            @Field("value") String value
    );
    @FormUrlEncoded
    @POST("/user/verify-otp-recovery")
    Call<ResponseBody> verifyotp(
            @Field("userId") int userId,
            @Field("newPassword") String newPassword,
            @Field("verifyCode") String verifyCode
    );
    @FormUrlEncoded
    @POST("user/login/by-google")
    Call<ResponseBody> logingg(
            @Field("accessToken") String accessToken
    );
    @FormUrlEncoded
    @POST("user/login/by-facebook")
    Call<ResponseBody> loginfb(
            @Field("accessToken") String accessToken
    );

    @GET("tour/list")
    Call<Tours> getListTour(
            @Header("Authorization") String token,
            @Query("rowPerPage") int pageSize,
            @Query("pageNum") int pageIndex,
            @Query("orderBy") String orderBy,
            @Query("isDesc") boolean isDesc
    );
    @GET("user/info")
    Call<ResponseBody> getUserInfo(
            @Header("Authorization") String token
    );

    @FormUrlEncoded
    @POST("tour/create")
    Call<ResponseBody> createTour(
            @Header("Authorization") String token,
            @Field("name") String name,
            @Field("startDate") long startDate,
            @Field("endDate") long endDate,
            @Field("sourceLat") Double sourceLat,
            @Field("sourceLong") Double sourceLong,
            @Field("desLat") Double desLat,
            @Field("desLong") Double desLong,
            @Field("isPrivate") boolean isPrivate,
            @Field("adults") Integer adults,
            @Field("childs") Integer childs,
            @Field("minCost") Integer minCost,
            @Field("maxCost") Integer maxCost,
            @Field("avatar") String avatar
    );
    @GET("tour/info")
    Call<TourInfo> getTourInfo(
            @Header("Authorization") String token,
            @Query("tourId") int tourId
    );
    @POST("tour/suggested-destination-list")
    Call<StopPoint> getSuggestedDestinationListOne(
            @Header("Authorization") String token,
            @Body GetOneSuggestSP getOneSuggestSP
            );
    @POST("tour/set-stop-points")
    Call<ResponseBody> addStopPointToTour(
            @Header("Authorization") String token,
            @Body AddStopPointRequest body
    );
    @FormUrlEncoded
    @POST("tour/add/member")
    Call<ResponseBody> addMemberToTour(
            @Header("Authorization") String token,
            @Field("tourId") String tourId,
            @Field("invitedUserId") String invitedUserId,
            @Field("isInvited") boolean isInvited
    );
    @GET("tour/search/service")
    Call<ResponseBody> searchDestination(
            @Field("searchKey") String keyword,
            @Field("provinceId") int provinceId,
            @Field("provinceName") String provinceName,
            @Field("pageIndex") int pageIndex,
            @Field("pageSize") int pageSize
    );
    @GET("tour/history-user")
    Call<Tours> getHistoryTourOfUser(
            @Header("Authorization") String token,
            @Query("pageIndex") int pageIndex,
            @Query("pageSize") String pageSize
    );
    @GET("/tour/get/invitation")
    Call<ListInvitation> getListInvitation(
            @Header("Authorization") String token,
            @Query("pageIndex") int pageIndex,
            @Query("pageSize") String pageSize
    );

    @FormUrlEncoded
    @POST("/tour/current-users-coordinate")
    Call<ResponseBody> getCoordinateUsersInTour(
            @Header("Authorization") String token,
            @Field("userId") String userId,
            @Field("tourId") String tourId,
            @Field("lat") double lat,
            @Field("long") double lng

    );
    @GET("/tour/notification-list")
    Call<Notifications> getNotificationOfATour(
            @Header("Authorization") String token,
            @Query("pageIndex") int pageIndex,
            @Query("pageSize") String pageSize
    );
    @GET("/tour/get/noti-on-road-coordinate")
    Call<Notifications> getNotificationOfATour(
            @Query("lat") double lat,
            @Query("long") double lng
    );
    @FormUrlEncoded
    @POST("/tour/add/notification-on-road")
    Call<ResponseBody> createNotiOnRoad(
            @Header("Authorization") String token,
            @Field("lat") double latitude,
            @Field("long") double longtitude,
            @Field("tourId") int tourId,
            @Field("userId") int userId,
            @Field("notificationType") Integer notiType,
            @Field("speed") Integer speed,
            @Field("note") String note
    );
    @FormUrlEncoded
    @POST("/tour/add/member")
    Call<ResponseBody> inviteMemberOrJoin(
            @Header("Authorization") String token,
            @Field("TourId") String tourId,
            @Field("invitedUserId") String idInvitedMember,
            @Field("isInvited") Boolean isPrivate
    );

    @FormUrlEncoded
    @POST("tour/update-tour")
    Call<ResponseBody> UpdateTour(
            @Header("Authorization") String token,
            @Field("id") String id,
            @Field("name") String name,
            @Field("startDate") Long startDate,
            @Field("endDate") Long endDate,
            @Field("sourceLat") Double sourceLat,
            @Field("sourceLong") Double sourceLong,
            @Field("desLat") Double desLat,
            @Field("desLong") Double desLong,
            @Field("isPrivate") Boolean isPrivate,
            @Field("adults") Integer adults,
            @Field("childs") Integer childs,
            @Field("minCost") Integer minCost,
            @Field("maxCost") Integer maxCost,
            @Field("avatar") String avatar,
            @Field("status") Integer status
    );

    @FormUrlEncoded
    @POST("/user/notification/put-token")
    Call<ResponseBody> registerFirebaseToken(
            @Header("Authorization") String token,
            @Field("fcmToken") String fcmToken,
            @Field("deviceId") String deviceId,
            @Field("platform") int platform,
            @Field("appVersion") String appVer
    );
    @FormUrlEncoded
    @POST("/user/notification/remove-token")
    Call<ResponseBody> removeFirebaseToken(
            @Header("Authorization") String token,
            @Field("fcmToken") String firebaseToken,
            @Field("deviceId") String devId
    );
    @FormUrlEncoded
    @POST("/tour/add/member")
    Call<ResponseBody> tourAddMember(
            @Header("Authorization") String token,
            @Field("tourId") String tourId,
            @Field("invitedUserId") String inviteUserId,
            @Field("isInvited") boolean isInvited
    );
    @GET("/tour/search/service")
    Call<StopPoints> getSearchStopPoint(
            @Header("Authorization") String token,
            @Query("searchKey") String searchKey,
            @Query("provinceId") int provinceId,
            @Query("provinceName") String provinceName,
            @Query("pageIndex") int pageIndex,
            @Query("pageSize") int pageSize
    );

    @GET("/user/search")
    Call<UserSearchResult> searchUser(
            @Query("searchKey") String searchKey,
            @Query("pageIndex") int pageIndex,
            @Query("pageSize") int pageSize
    );

    @GET("/tour/get/review-point-stats")
    Call<PointStats> getReviewStar(
            @Header("Authorization") String token,
            @Query("tourId") int tourId
    );

    @GET("/tour/get/review-list")
    Call<Reviewers> getReviewList(
            @Header("Authorization") String token,
            @Query("tourId") int tourId,
            @Query("pageIndex") int pageIndex,
            @Query("pageSize") int pageSize
    );

    @FormUrlEncoded
    @POST("/tour/add/review")
    Call<MessageResponseRetrofit> postReview(
            @Header("Authorization") String token,
            @Field("tourId") int tourId,
            @Field("point") String point ,
            @Field("review") String review
    );
    @FormUrlEncoded
    @POST("/tour/response/invitation")
    Call<ResponseBody> responseToAnInvitation(
            @Header("Authorization") String token,
            @Field("tourId") String tourId,
            @Field("isAccepted") boolean isAccepted
    );
    @GET("/tour/get/feedback-point-stats")
    Call<PointStats> getPointService(
            @Header("Authorization") String token,
            @Query("serviceId") int serviceId
    );
    @GET("/tour/get/feedback-service")
    Call<Feedbacks> getFeedbackService(
            @Header("Authorization") String token,
            @Query("serviceId") int serviceId,
            @Query("pageIndex") int pageIndex,
            @Query("pageSize") int pageSize
    );
    @FormUrlEncoded
    @POST("/tour/add/feedback-service")
    Call<MessageResponseRetrofit> postFeedbackService(
            @Header("Authorization") String token,
            @Field("serviceId") int serviceId,
            @Field("point") String point,
            @Field("feedback") String feedback
    );
    @FormUrlEncoded
    @POST("/tour/current-users-coordinate")
    Call<ArrayList<MemberCoordinate>> getCoordinateMember(
            @Header("Authorization") String token,
            @Field("userId") String userId,
            @Field("tourId") String tourId ,
            @Field("lat") double latitude,
            @Field("long") double longtitude
    );


    @GET("/tour/get/noti-on-road-coordinate")
    Call<NotiList> getNotiOnRoadByCoordinate(
            @Header("Authorization") String token,
            @Query("lat") double lat,
            @Query("long") double lng,
            @Query("notificationType") int notiType
    );
    @FormUrlEncoded
    @POST("/tour/report/feedback")
    Call<ResponseBody> postReportFeedback(
            @Header("Authorization") String token,
            @Field("feedbackId") String feedbackId
    );
    @FormUrlEncoded
    @POST("/tour/report/review")
    Call<ResponseBody> postReportReview(
            @Header("Authorization") String token,
            @Field("reviewId") String reviewId
    );
    @Multipart
    @POST("/tour/recording")
    Call<ResponseBody> postRecording(
            @Header("Authorization") String token,
            @Part MultipartBody.Part file,
            @Part("tourId") RequestBody tourId,
            @Part("fullName") RequestBody fullName,
            @Part("avatar") RequestBody avatar,
            @Part("lat") RequestBody lat,
            @Part("long") RequestBody lng
    );
}
