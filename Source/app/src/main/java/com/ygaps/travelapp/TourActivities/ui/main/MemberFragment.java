package com.ygaps.travelapp.TourActivities.ui.main;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.ygaps.travelapp.MyApplication;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.TourActivities.SearchAndInviteUser;
import com.ygaps.travelapp.data.model.TourInfo;
import com.ygaps.travelapp.retrofit.RetrofitClient;

import java.io.IOException;
import java.lang.reflect.Member;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MemberFragment extends Fragment {
  // TODO: Rename parameter arguments, choose names that match
  // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
  public MemberFragment() {
    // Required empty public constructor
  }

  private PageViewModel pageViewModel;
  private RecyclerView recyclerView;
  private Button invite;
  private ArrayList<TourInfo.Member> members = new ArrayList<>();
  public static MemberFragment newInstance() {
    MemberFragment fragment = new MemberFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    pageViewModel = ViewModelProviders.of(getActivity()).get(PageViewModel.class);

  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_member,container,false);
    recyclerView = view.findViewById(R.id.member);

    recyclerView.setHasFixedSize(true);
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
    recyclerView.setLayoutManager(linearLayoutManager);

    final MembersListAdapter membersListAdapter = new MembersListAdapter();
    recyclerView.setAdapter(membersListAdapter);
    invite = view.findViewById(R.id.invite);
    final int history = pageViewModel.getHistory();
    pageViewModel.getObserveable().observe(getViewLifecycleOwner(), new Observer<TourInfo>() {
      @Override
      public void onChanged(final TourInfo tourInfo) {
        if (tourInfo!=null) {
          members = tourInfo.getMembers();
          membersListAdapter.notifyDataSetChanged();
          if (history==1) {
            invite.setOnClickListener(new OnClickListener() {
              @Override
              public void onClick(View v) {
                Intent intent = new Intent(getContext(), SearchAndInviteUser.class);
                intent.putExtra("tourId", tourInfo.getId());
                startActivity(intent);
              }
            });
          } else
          {
            invite.setText(R.string.join_tour);
            invite.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Call call = RetrofitClient
                            .getInstance()
                            .getApi()
                            .addMemberToTour(MyApplication.token, String.valueOf(tourInfo.getId()), MyApplication.userId, false);
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                Toast.makeText(getContext(), R.string.success_join, Toast.LENGTH_LONG).show();
                            } else {
                                try {
                                    Toast.makeText(getContext(), response.errorBody().string(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                        }
                    });
                }
            });
          }
        }
      }
    });
    return view;
  }



  private class MembersListAdapter extends RecyclerView.Adapter<MemberViewHolder>
  {
    @NonNull
    @Override
    public MemberViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
      View viewItem = layoutInflater.inflate(R.layout.recycler_members, parent, false);
      return new MemberViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull MemberViewHolder holder, int position) {
      Picasso.get().load(members.get(position).getAvatar()).error(R.drawable.img_default).into(holder.ava);
      holder.name.setText(members.get(position).getName());
      if (members.get(position).isHost())
      {
        holder.isHost.setVisibility(View.VISIBLE);
      }
      else
      {
        holder.isHost.setVisibility(View.INVISIBLE);
      }
    }

    @Override
    public int getItemCount() {
      return members.size();
    }
  }
  private class MemberViewHolder extends RecyclerView.ViewHolder
  {
    private CircleImageView ava;
    private TextView name;
    private TextView isHost;
    public MemberViewHolder(@NonNull View itemView) {
      super(itemView);
      ava = (CircleImageView) itemView.findViewById(R.id.avatar);
      name = (TextView)itemView.findViewById(R.id.name);
      isHost = (TextView)itemView.findViewById(R.id.admintour);
    }

    @Override
    public String toString() {
      return super.toString();
    }
  }
}
