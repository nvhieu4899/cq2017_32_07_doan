package com.ygaps.travelapp.data.model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserSearchResult {
  @SerializedName("total")
  @Expose
  private int total;
  @SerializedName("users")
  @Expose
  private ArrayList<UserSearch> userSearches;
  public class UserSearch
  {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("gender")
    @Expose
    private int gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("avatar")
    @Nullable
    @Expose
    private String avatar;
    @SerializedName("typeLogin")
    @Expose
    private int typeLogin;

    public int getId() {
      return id;
    }

    public void setId(int id) {
      this.id = id;
    }

    public String getFullName() {
      return fullName;
    }

    public void setFullName(String fullName) {
      this.fullName = fullName;
    }

    public String getEmail() {
      return email;
    }

    public void setEmail(String email) {
      this.email = email;
    }

    public String getPhone() {
      return phone;
    }

    public void setPhone(String phone) {
      this.phone = phone;
    }

    public int getGender() {
      return gender;
    }

    public void setGender(int gender) {
      this.gender = gender;
    }

    public String getDob() {
      return dob;
    }

    public void setDob(String dob) {
      this.dob = dob;
    }

    @Nullable
    public String getAvatar() {
      return avatar;
    }

    public void setAvatar(@Nullable String avatar) {
      this.avatar = avatar;
    }

    public int getTypeLogin() {
      return typeLogin;
    }

    public void setTypeLogin(int typeLogin) {
      this.typeLogin = typeLogin;
    }
  }

  public int getTotal() {
    return total;
  }

  public void setTotal(int total) {
    this.total = total;
  }

  public ArrayList<UserSearch> getUserSearches() {
    return userSearches;
  }

  public void setUserSearches(ArrayList<UserSearch> userSearches) {
    this.userSearches = userSearches;
  }
}
