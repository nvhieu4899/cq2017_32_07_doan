package com.ygaps.travelapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.data.model.UserSearchResult;
import com.ygaps.travelapp.retrofit.RetrofitClient;
import com.ygaps.travelapp.ui.login.LoginActivity;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Forgot extends AppCompatActivity {
   private TextInputLayout inputLayout;
   private TextInputLayout newpass;
   private TextInputLayout otp;
   private Button btn_xacnhan;
   private Button fg_otpbt;
   private TextView fg_log;
   private TextView fg_sign;
   private boolean isAccept = false;
   private volatile UserSearchResult.UserSearch userSearches = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);
        inputLayout = (TextInputLayout)findViewById(R.id.forgot_input);
        newpass = (TextInputLayout)findViewById(R.id.forgot_newpass);
        otp = (TextInputLayout)findViewById(R.id.forgot_otp);
        btn_xacnhan = (Button)findViewById(R.id.forgot_accept);
        fg_log = (TextView)findViewById(R.id.forgot_log);
        fg_sign = (TextView)findViewById(R.id.forgot_sign);
        fg_otpbt = (Button) findViewById(R.id.forgot_get_otp);
        btn_xacnhan = (Button) findViewById(R.id.forgot_accept);

        fg_sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SignUp.class);
                startActivity(intent);
                finish();
            }
        });

        fg_log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        fg_otpbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputString = inputLayout.getEditText().getText().toString().trim();
                switch (valid(inputString)){
                    case 0: return;
                    case 1: callRetrofit("email",inputString);
                        break;
                    case 2: callRetrofit("phone",inputString);
                        break;
                }
            }
        });

        btn_xacnhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputString = inputLayout.getEditText().getText().toString().trim();
                if (valid(inputString) != 0 & validotp() & validnewpass()) {
                    Call call = RetrofitClient.getInstance().getApi().searchUser(inputString, 1, 1);
                    call.enqueue(new Callback<UserSearchResult>() {
                        @Override
                        public void onResponse(Call<UserSearchResult> call, Response<UserSearchResult> response) {
                            if (response.isSuccessful()) {
                                userSearches = response.body().getUserSearches().get(0);
                                int id = userSearches.getId();
                                String newpassstr = newpass.getEditText().getText().toString().trim();
                                String otpstr = otp.getEditText().getText().toString().trim();
                                Call call2 = RetrofitClient
                                        .getInstance()
                                        .getApi()
                                        .verifyotp(id, newpassstr, otpstr);
                                call2.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        String res = null;
                                        try {
                                            if (response.code() == 200) {
                                                Toast.makeText(getApplicationContext(), "Mật khẩu đã đặt lại thành công", Toast.LENGTH_LONG).show();
                                                finish();
                                            } else {
                                                res = response.errorBody().string();
                                            }
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        if (res != null) {
                                            try {
                                                JSONObject jsonObject = new JSONObject(res);
                                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(Call<UserSearchResult> call, Throwable t) {

                        }
                    });

                }

            }

        });
    }

    private void sendreq() {

    }

    private int valid(String inputString){
        if (Patterns.EMAIL_ADDRESS.matcher(inputString).matches())
        {
            inputLayout.setError(null);
            return 1;
        } else if (Patterns.PHONE.matcher(inputString).matches()){
            inputLayout.setError(null);
            return 2;
        }
        inputLayout.setError(getString(R.string.str_invalidusername));
        return 0;
    }

    private Boolean validotp() {
        String otpstr = otp.getEditText().getText().toString().trim();
        if (otpstr.isEmpty()) {
            otp.setError(getString(R.string.str_required));
            return false;
        } else {
            otp.setError(null);
        }
        return true;
    }

    private Boolean validnewpass() {
        String newpassstr = newpass.getEditText().getText().toString().trim();
        if (newpassstr.isEmpty()) {
            newpass.setError(getString(R.string.str_required));
            return false;
        } else {
            newpass.setError(null);
        }
        return true;
    }

    private void callRetrofit(String type, String value)
    {
        retrofit2.Call call = RetrofitClient
                .getInstance()
                .getApi()
                .forgotUser(type,value);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String res=null;
                try {
                    if (response.code() == 200) {
                        Toast.makeText(getApplicationContext(),"Đã gửi mã xác nhận",Toast.LENGTH_LONG).show();
                        fg_otpbt.setText(R.string.resend_otp);
                    } else {
                        res = response.errorBody().string();
                    }
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
                if (res!=null)
                {
                    try{
                        JSONObject jsonObject = new JSONObject(res);
                        Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

}
