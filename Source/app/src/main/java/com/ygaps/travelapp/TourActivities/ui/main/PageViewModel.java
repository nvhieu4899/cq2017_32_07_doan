package com.ygaps.travelapp.TourActivities.ui.main;

import android.app.Application;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.ygaps.travelapp.data.model.TourInfo;

import io.grpc.internal.SharedResourceHolder;

public class PageViewModel extends ViewModel {


  private MutableLiveData<TourInfo> observeable;
  private int history;
  public void setTour(TourInfo tourInfo)
  {
    observeable.setValue(tourInfo);
  }

  public int getHistory() {
    return history;
  }

  public void setHistory(int history) {
    this.history = history;
  }

  public PageViewModel() {
    super();
    observeable = new MutableLiveData<>();
  }

  public MutableLiveData<TourInfo> getObserveable() {
    return observeable;
  }

  public void setObserveable(MutableLiveData<TourInfo> observeable) {
    this.observeable = observeable;
  }
}