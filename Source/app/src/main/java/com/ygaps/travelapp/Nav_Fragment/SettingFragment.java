package com.ygaps.travelapp.Nav_Fragment;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.ygaps.travelapp.MyApplication;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.retrofit.RetrofitClient;
import com.ygaps.travelapp.ui.login.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment {

    private Button logoutBtn;
    private Button editAccountBtn;
    private TextView nameText;
    private TextView fullNameText;
    private TextView genderText;
    private TextView dateBirthText;
    private TextView phoneText;
    private TextView emailText;
    private TextView addressText;

    private CircleImageView avatarImg;
    private CircleImageView editAvatarBtn;
    String ava_Based64 = "";
    String dob;

    String token;

    public static final int PICK_IMAGE = 1;

    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        logoutBtn = (Button)view.findViewById(R.id.logout_btn);
        editAccountBtn = view.findViewById(R.id.edit_account_btn);
        nameText = view.findViewById(R.id.name_text);
        fullNameText = view.findViewById(R.id.fullname_text);
        dateBirthText = view.findViewById(R.id.dateBirth_text);
        genderText = view.findViewById(R.id.gender_text);
        addressText = view.findViewById(R.id.address_text);
        phoneText = view.findViewById(R.id.phone_text);
        emailText = view.findViewById(R.id.email_text);

        avatarImg = view.findViewById(R.id.profile_image);
        editAvatarBtn = view.findViewById(R.id.edit_image_btn);

        SharedPreferences sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences("token", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");

        Call call = RetrofitClient.getInstance().getApi().getUserInfo(token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.body() != null) {
                    if (response.isSuccessful()) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            String fullName = jsonObject.getString("fullName");
                            String phone = jsonObject.getString("phone");
                            String address = jsonObject.getString("address");
                            int gender = jsonObject.getInt("gender");
                            dob = jsonObject.getString("dob");
                            String email = jsonObject.getString("email");

                            // format string dob lại dd/mm/yyyy
                            if(dob.equals("null")) {
                                dateBirthText.setText("Chưa có");
                            }
                            else {
                                String dobFormated = dob.substring(0, 10);
                                dateBirthText.setText(dobFormated);
                            }

                            nameText.setText(fullName);
                            fullNameText.setText(fullName);
                            phoneText.setText(phone);
                            addressText.setText(address);
                            if(gender == 1) {
                                genderText.setText("Nam");
                            }
                            else {
                                genderText.setText("Nữ");
                            }


                            emailText.setText(email);
                        }
                        catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }

                    }
                    else {
                        if (response.code()==401) {
                            Toast.makeText(getActivity(),getString(R.string.token_auth_failed),Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getActivity(),LoginActivity.class));
                            getActivity().finish();
                        }
                        else {
                            Toast.makeText(getActivity(),getString(R.string.getUserInfo_503),Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getActivity(),LoginActivity.class));
                            getActivity().finish();

                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                startActivity(new Intent(getActivity(),LoginActivity.class));
                getActivity().finish();
            }
        });

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences("token", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove("token");
                editor.apply();
                Call call1 = RetrofitClient.getInstance().getApi().removeFirebaseToken(MyApplication.token,MyApplication.FCMtoken,MyApplication.deviceId);
                call1.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Toast.makeText(getContext(),getString(R.string.success_logout),Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        editAccountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayEditAccDialog();
            }
        });

        editAvatarBtn.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("IntentReset")
            @Override
            public void onClick(View v) {
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getIntent.setType("image/*");

                @SuppressLint("IntentReset") Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");

                Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

                startActivityForResult(chooserIntent, PICK_IMAGE);
            }
        });
        return view;
    }

    public void displayEditAccDialog() {
        LayoutInflater layoutInflater = getLayoutInflater();
        final View dialogEditAccView = layoutInflater.inflate(R.layout.dialog_edit_account,null);

        final TextInputLayout nameInput = (TextInputLayout) dialogEditAccView.findViewById(R.id.name);
        final TextInputLayout emailInput = (TextInputLayout) dialogEditAccView.findViewById(R.id.email);
        final TextInputLayout adressInput = (TextInputLayout) dialogEditAccView.findViewById(R.id.adress);
        final TextInputLayout phoneInput= (TextInputLayout) dialogEditAccView.findViewById(R.id.phone);
        final RadioGroup genderRadioBtn = (RadioGroup) dialogEditAccView.findViewById(R.id.radio_gender_group);
        final TextView birthDay = dialogEditAccView.findViewById(R.id.day_of_birth);

        // fill value
        genderRadioBtn.check(R.id.edit_gender_female);
        nameInput.getEditText().setText(fullNameText.getText().toString());
        emailInput.getEditText().setText(emailText.getText().toString());
        phoneInput.getEditText().setText(phoneText.getText().toString());
        adressInput.getEditText().setText(addressText.getText().toString());
        birthDay.setText(dateBirthText.getText());
        String genderStrTemp = genderText.getText().toString();
        if(genderStrTemp.equals("Nam")) {
            genderRadioBtn.check(R.id.edit_gender_male);
        }

        // edit
        birthDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePick(birthDay);
            }
        });


        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setView(dialogEditAccView);


        alert.setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int which) {
                final String email = emailInput.getEditText().getText().toString();
                final String phone  = phoneInput.getEditText().getText().toString();
                final String name = nameInput.getEditText().getText().toString();

                final int gender;

                if (genderRadioBtn.getCheckedRadioButtonId() == R.id.edit_gender_male){
                    gender = 1;
                }
                else {
                    gender = 0;
                }

                retrofit2.Call callPostEditAcc = RetrofitClient
                        .getInstance()
                        .getApi()
                        .editUser(token, name, email, phone, dob, gender);
                callPostEditAcc.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            JSONObject jsonObject = null;
                            try {
                                assert response.body() != null;
                                jsonObject = new JSONObject(response.body().string());
                                String message = jsonObject.getString("message");

                                nameText.setText(name);
                                fullNameText.setText(name);
                                phoneText.setText(phone);
                                emailText.setText(email);
                                genderText.setText(gender == 1 ? "Nam": "Nữ");
                                dateBirthText.setText(dob);

                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            try {
                                if (response.errorBody() != null) {
                                    JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                    String message = jsonObject.getString("message");
                                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                                }
                            } catch (IOException | JSONException e) {
                                e.printStackTrace();
                            }
                            Toast.makeText(getActivity(), getString(R.string.str_failure_edit), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        dialog.dismiss();
                    }
                });

            }
        });
        alert.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        final AlertDialog dialog = alert.create();
        dialog.show();
    }

    private void datePick(final TextView v) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year, month, dayOfMonth);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                v.setText(simpleDateFormat.format(calendar.getTime()));
                dob = simpleDateFormat.format(calendar.getTime());
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    class LoadImage extends AsyncTask<Void,Void, Bitmap>{
        private Uri imagePath;

        LoadImage(Uri imagePath) {
            this.imagePath = imagePath;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                InputStream imageStream = getActivity().getApplicationContext().getContentResolver().openInputStream(imagePath);

                return BitmapFactory.decodeStream(imageStream);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                final InputStream imageStream = getActivity().getApplicationContext().getContentResolver().openInputStream(imagePath);
                byte[] bytes;
                byte[] buffer = new byte[8192];
                int bytesRead;
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                try {
                    while ((bytesRead = imageStream.read(buffer)) != -1) {
                        output.write(buffer, 0, bytesRead);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bytes = output.toByteArray();
                ava_Based64 = Base64.encodeToString(bytes, Base64.DEFAULT);
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
                Toast.makeText(getActivity(),"Something went wrong",Toast.LENGTH_LONG).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            avatarImg.setImageBitmap(bitmap);
        }
    }
}
