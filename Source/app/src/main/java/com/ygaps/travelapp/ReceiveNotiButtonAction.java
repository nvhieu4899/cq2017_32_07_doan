package com.ygaps.travelapp;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.firebase.messaging.FirebaseMessaging;
import com.squareup.okhttp.ResponseBody;
import com.ygaps.travelapp.Services.LocationSender;
import com.ygaps.travelapp.retrofit.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceiveNotiButtonAction extends BroadcastReceiver {
  private NotificationManager notificationManager;
  @Override
  public void onReceive(final Context context, Intent intent) {
    notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
    String action = intent.getAction();
    if (MyApplication.YES_ACTION.equals(action))
    {
      final String tourId = intent.getStringExtra("tourId");
      Call call = RetrofitClient.getInstance().getApi().responseToAnInvitation(MyApplication.token,tourId,true);
      call.enqueue(new Callback<ResponseBody>(){
        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
          notificationManager.cancel(Integer.valueOf(tourId));
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {

        }
      });

    }
    else if (MyApplication.NO_ACTION.equals(action))
    {
      final String tourId = intent.getStringExtra("tourId");

      Call call = RetrofitClient.getInstance().getApi().responseToAnInvitation(MyApplication.token,tourId,false);
      call.enqueue(new Callback<ResponseBody>(){
        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
          notificationManager.cancel(Integer.valueOf(tourId));
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {

        }
      });
    }
    else if (MyApplication.STOP_TRACKING_ACTION.equals(action))
    {
      String tourid = intent.getStringExtra("tourId");
      FirebaseMessaging.getInstance().unsubscribeFromTopic("/topics/tour-id-"+tourid);
      context.stopService(new Intent(context, LocationSender.class));
    }
  }
}
