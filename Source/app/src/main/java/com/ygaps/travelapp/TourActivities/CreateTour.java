package com.ygaps.travelapp.TourActivities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ygaps.travelapp.R;
import com.ygaps.travelapp.retrofit.RetrofitClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateTour extends AppCompatActivity {
    private TextInputLayout name;
    private TextView startdate;
    private TextView enddate;
    private RadioGroup radioGroup;
    private TextInputLayout adults;
    private TextInputLayout childs;
    private TextInputLayout minCost;
    private TextInputLayout maxCost;
    private Button createbt;

    private TextInputLayout sourceadd;
    private TextInputLayout desadd;
    private LatLng source = null;
    private LatLng des = null;
    private Button avatarpick;
    private ImageView avapickIMG;
    String ava_Based64 = null;
    public static final int PICK_IMAGE = 1;
    public static final int PICK_MAP = 2;
    private Button mapPick;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_tour);
        name = (TextInputLayout) findViewById(R.id.createtour_name);
        startdate = (TextView) findViewById(R.id.createtour_startdate);
        enddate = (TextView) findViewById(R.id.createtour_enddate);

        sourceadd = (TextInputLayout) findViewById(R.id.createtour_sourceadd);
        desadd = (TextInputLayout) findViewById(R.id.createtour_desadd);
        radioGroup = (RadioGroup) findViewById(R.id.radio_private_group);


        radioGroup.check(R.id.createtour_private_yes);
        adults = (TextInputLayout) findViewById(R.id.createtour_adults);
        childs = (TextInputLayout) findViewById(R.id.createtour_childs);
        minCost = (TextInputLayout) findViewById(R.id.createtour_minCost);
        maxCost = (TextInputLayout) findViewById(R.id.createtour_maxCost);
        createbt = (Button) findViewById(R.id.createtour_button);

        mapPick = (Button) findViewById(R.id.map_pick);
        mapPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputEditText inputSrc = (TextInputEditText) findViewById(R.id.srcInput);
                TextInputEditText inputDes = (TextInputEditText) findViewById(R.id.desInput);

                Intent intent = new Intent(getApplicationContext(), PickSourceAndDestination.class);
                LatLng srcLatLng, desLatLng;
                srcLatLng = getLocationFromAddress(getApplicationContext(), inputSrc.getText().toString());
                desLatLng = getLocationFromAddress(getApplicationContext(), inputDes.getText().toString());
                if (srcLatLng != null) {
                    intent.putExtra("startPointLat", srcLatLng.latitude);
                    intent.putExtra("startPointLong", srcLatLng.longitude);
                }
                if (desLatLng != null) {
                    intent.putExtra("endPointLat", desLatLng.latitude);
                    intent.putExtra("endPointLong", desLatLng.longitude);
                }
                startActivityForResult(intent, PICK_MAP);
            }
        });


        avatarpick = (Button) findViewById(R.id.pickAva_tour_create);
        avatarpick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getIntent.setType("image/*");

                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");

                Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

                startActivityForResult(chooserIntent, PICK_IMAGE);
            }
        });
        startdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePick(startdate);
            }
        });
        avapickIMG = (ImageView) findViewById(R.id.avaPick);
        enddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePick(enddate);
            }
        });

        createbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createbt.setEnabled(false);
                if (validname() & validstartdate() & validenddate() & validsource() & validdes() & validcost()) {
                    SharedPreferences sharedPreferences = getSharedPreferences("token", Context.MODE_PRIVATE);
                    String token = sharedPreferences.getString("token", null);
                    String namestr = name.getEditText().getText().toString().trim();
                    String startdatestr = startdate.getText().toString().trim();
                    final long startmili = getMilli(startdatestr);
                    String enddatestr = enddate.getText().toString().trim();
                    long endmili = getMilli(enddatestr);
                    boolean pri = true;
                    if (radioGroup.getCheckedRadioButtonId() == R.id.createtour_private_no)
                        pri = false;
                    Integer adu = null;
                    String adustr = adults.getEditText().getText().toString().trim();
                    if (!adustr.isEmpty()) adu = Integer.parseInt(adustr);
                    Integer chi = null;
                    String chistr = childs.getEditText().getText().toString().trim();
                    if (!chistr.isEmpty()) chi = Integer.parseInt(chistr);
                    Integer min = null;
                    String minstr = minCost.getEditText().getText().toString().trim();
                    if (!minstr.isEmpty()) min = Integer.parseInt(minstr);
                    Integer max = null;
                    String maxstr = maxCost.getEditText().getText().toString().trim();
                    if (!maxstr.isEmpty()) max = Integer.parseInt(maxstr);

                    retrofit2.Call call;
                    call = RetrofitClient
                            .getInstance()
                            .getApi()
                            .createTour(token, namestr, startmili, endmili, source.latitude, source.longitude, des.latitude, des.longitude, pri, adu, chi, min, max, ava_Based64);

                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            String res = null;
                            try {
                                if (response.code() == 200) {
                                    int tourId =-1;
                                    Toast.makeText(getApplicationContext(), R.string.success_create_tour, Toast.LENGTH_LONG).show();
                                    String s = response.body().string();
                                    try {
                                        JSONObject jsonObject = new JSONObject(s);
                                        tourId = Integer.parseInt(jsonObject.getString("id"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    Intent intent = new Intent(getApplicationContext(), AddStopPointActivity.class);
                                    intent.putExtra("startPointLat", source.latitude);
                                    intent.putExtra("startPointLong", source.longitude);
                                    intent.putExtra("endPointLat", des.latitude);
                                    intent.putExtra("endPointLong", des.longitude);
                                    intent.putExtra("tourId",tourId);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    res = response.errorBody().string();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if (res != null) {
                                try {
                                    JSONObject jsonObject = new JSONObject(res);
                                    Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
                createbt.setEnabled(true);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                    final Uri imageUri = data.getData();
                    new LoadImage(imageUri).execute();
                                }
        }
        if (requestCode == PICK_MAP && resultCode == Activity.RESULT_OK) {

            LatLng src = new LatLng(data.getDoubleExtra("srcLat", 0), data.getDoubleExtra("srcLng", 0));
            LatLng des = new LatLng(data.getDoubleExtra("desLat", 0), data.getDoubleExtra("desLng", 0));

            TextInputEditText inputSrc = (TextInputEditText) findViewById(R.id.srcInput);
            TextInputEditText inputDes = (TextInputEditText) findViewById(R.id.desInput);

            if (getAddressFromLatLng(src) != null) {
                inputSrc.setText(getFullAddressStr(getAddressFromLatLng(src)));
            }

            if (getAddressFromLatLng(des) != null) {
                inputDes.setText(getFullAddressStr(getAddressFromLatLng(des)));
            }
        }
    }

    public static String getFullAddressStr(List<Address> list) {
        StringBuilder builder = new StringBuilder();
        Address address = list.get(0);
        for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
            builder.append(address.getAddressLine(i)).append(", ");
        }
        return builder.toString();
    }
    private void datePick(final TextView v) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year, month, dayOfMonth);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-YYYY");
                v.setText(simpleDateFormat.format(calendar.getTime()));
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    private long getMilli(String datefm) {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        try {
            date = formatter.parse(datefm);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    private Boolean validname() {
        String namestr = name.getEditText().getText().toString().trim();
        if (namestr.isEmpty()) {
            name.setError(getString(R.string.str_required));
            return false;
        } else {
            name.setError(null);
        }
        return true;
    }

    private List<Address> getAddressFromLatLng(LatLng latLng) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses.isEmpty())
                return null;// Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
            return addresses;
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Không thể lấy dịa chỉ", Toast.LENGTH_LONG).show();
            return null;
        }

    }

    private Boolean validstartdate() {
        String startstr = startdate.getText().toString().trim();
        if (startstr == getString(R.string.createtour_choose)) {
            startdate.setError(getString(R.string.str_required));
            return false;
        } else {
            startdate.setError(null);
        }
        return true;
    }

    private Boolean validenddate() {
        String endstr = enddate.getText().toString().trim();
        String stastr = startdate.getText().toString().trim();
        if (endstr == getString(R.string.createtour_choose)) {
            enddate.setError(getString(R.string.str_required));
            return false;
        } else if (stastr != getString(R.string.createtour_choose)) {
            long s = getMilli(stastr);
            long e = getMilli(endstr);
            if (e < s) {
                enddate.setError(getString(R.string.date_error));
                return false;
            } else enddate.setError(null);
        }
        return true;
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);

            if (address.isEmpty()) {
                return null;
            }

            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return p1;
    }

    private Boolean validsource() {
        String soustr = sourceadd.getEditText().getText().toString().trim();
        if (soustr.isEmpty()) {
            sourceadd.setError(getString(R.string.str_required));
            return false;
        } else {
            if (getLocationFromAddress(this, soustr) == null) {
                sourceadd.setError(getString(R.string.error_add));
                return false;
            } else {
                source = getLocationFromAddress(this, soustr);
                sourceadd.setError(null);
            }
        }
        return true;
    }

    private Boolean validdes() {
        String desstr = desadd.getEditText().getText().toString().trim();
        if (desstr.isEmpty()) {
            desadd.setError(getString(R.string.str_required));
            return false;
        } else {
            if (getLocationFromAddress(this, desstr) == null) {
                desadd.setError(getString(R.string.error_add));
                return false;
            } else {
                des = getLocationFromAddress(this, desstr);
                desadd.setError(null);
            }
        }
        return true;
    }

    private Boolean validcost() {
        String min = minCost.getEditText().getText().toString().trim();
        String max = maxCost.getEditText().getText().toString().trim();
        if (!min.isEmpty() && !max.isEmpty())
        {
            int minint = Integer.parseInt(min);
            int maxint = Integer.parseInt(max);
            if (maxint<minint) {
                minCost.setError("Không lớn hơn chi phí tối đa");
                return false;
            }
        }
        return true;
    }

    class LoadImage extends AsyncTask<Void,Void,Bitmap>
    {
        private Uri imagePath;

        LoadImage(Uri imagePath) {
            this.imagePath = imagePath;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                InputStream imageStream = getContentResolver().openInputStream(imagePath);

                return BitmapFactory.decodeStream(imageStream);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                final InputStream imageStream = getContentResolver().openInputStream(imagePath);
                byte[] bytes;
                byte[] buffer = new byte[8192];
                int bytesRead;
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                try {
                    while ((bytesRead = imageStream.read(buffer)) != -1) {
                        output.write(buffer, 0, bytesRead);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bytes = output.toByteArray();
                ava_Based64 = Base64.encodeToString(bytes, Base64.DEFAULT);
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(),"Something went wrong",Toast.LENGTH_LONG).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            avapickIMG.setImageBitmap(bitmap);
        }
    }

}