package com.ygaps.travelapp.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit retrofit = null;
    private static RetrofitClient mInstance;

    public static Retrofit getClient(String baseUrl) {
        if (retrofit == null) {

            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    private RetrofitClient()
    {
        retrofit = new Retrofit.Builder()
                .baseUrl(APIUtils.Base_Url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    public static synchronized RetrofitClient getInstance()
    {
        if (mInstance==null)
        {
            mInstance = new RetrofitClient();
        }
        return mInstance;
    }
    public DataClient getApi()
    {
        return retrofit.create(DataClient.class);
    }
}
