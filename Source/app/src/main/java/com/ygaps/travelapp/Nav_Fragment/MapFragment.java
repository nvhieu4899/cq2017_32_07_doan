package com.ygaps.travelapp.Nav_Fragment;


import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.Marker;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.ygaps.travelapp.MyApplication;
import com.ygaps.travelapp.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ygaps.travelapp.data.model.Feedback;
import com.ygaps.travelapp.data.model.Feedbacks;
import com.ygaps.travelapp.data.model.MessageResponseRetrofit;
import com.ygaps.travelapp.data.model.PointStats;
import com.ygaps.travelapp.data.model.StopPoint;
import com.ygaps.travelapp.data.model.StopPoints;
import com.ygaps.travelapp.main.FeedbackAdapter;
import com.ygaps.travelapp.retrofit.APIUtils;
import com.ygaps.travelapp.retrofit.DataClient;
import com.ygaps.travelapp.retrofit.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.ygaps.travelapp.TourActivities.PickSourceAndDestination.bitmapDescriptorFromVector;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment implements LocationListener{
    String provider;
    private LocationManager locationManager;
    private Spinner provinceSpinner;
    private ImageButton searchBtn;
    private TextInputEditText searchInput;
    String searchKey;
    String provinceName;
    int provinceId;

    DataClient dataClient;
    private PointStats pointStats;
    private Feedbacks feedbacks;

    private ArrayList<StopPoint> currentStopPoint = new ArrayList<>();
    private List<Feedback> feedbacksData = new ArrayList<>();
    private StopPoints stopPoints;
    private FeedbackAdapter feedbackAdapter;

    private boolean isLoading = false;
    private int loadedPage = 1;
    private int totalsize = 0;
    private final int PAGE_SIZE = 10;
    private final int MAX_SIZE = 100;
    private int stopPointId;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(getContext())
                        .setTitle(R.string.title_location_permission)
                        .setMessage(R.string.text_location_permission)
                        .setPositiveButton(R.string.btn_sure, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    private void addSPMarker(StopPoint sp)
    {
        Marker marker;
        if (sp.getServiceTypeId()==1)
            marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(sp.getLatitude()),Double.parseDouble(sp.getLongtitude())))
                    .title(sp.getName())
                    .draggable(false).icon(
                            bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_restaurant)
                    ).anchor(0.5f,0.5f));
        else if (sp.getServiceTypeId()==2)
            marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(sp.getLatitude()),Double.parseDouble(sp.getLongtitude())))
                    .title(sp.getName())
                    .draggable(false).icon(
                            bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_hotel)
                    ).anchor(0.5f,0.5f));
        else if (sp.getServiceTypeId()==3)
            marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(sp.getLatitude()),Double.parseDouble(sp.getLongtitude())))
                    .title(sp.getName())
                    .draggable(false).icon(
                            bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_fuel)
                    ).anchor(0.5f,0.5f));
        else
            marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(sp.getLatitude()),Double.parseDouble(sp.getLongtitude())))
                    .title(sp.getName())
                    .draggable(false).icon(
                            bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_pin)
                    ).anchor(0.5f,0.5f));
        marker.setTag(sp);
    }
    public void createStopPointsOnMap(List<StopPoint> stopPoints) {
        if (stopPoints.size()==0) return;
        for(int i = 0; i< stopPoints.size(); i++) {
            addSPMarker(stopPoints.get(i));
        }
        double first_latStopPoint = Double.parseDouble(stopPoints.get(0).getLatitude());
        double first_longStopPoint = Double.parseDouble(stopPoints.get(0).getLongtitude());
        LatLng firstStopPoint = new LatLng(first_latStopPoint, first_longStopPoint);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(firstStopPoint));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10));

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                displaySPInfoDialog(marker);
                return false;
            }
        });

    }
    public void displaySPInfoDialog(final Marker marker)
    {
        final StopPoint stopPoint = (StopPoint)marker.getTag();
        stopPointId = stopPoint.getId();
        LayoutInflater layoutInflater = getLayoutInflater();
        final View dialogSpInfo = layoutInflater.inflate(R.layout.info_stop_point_dialog_explore,null);
        // controls in view
        final TextInputLayout name = (TextInputLayout) dialogSpInfo.findViewById(R.id.name);
        final TextInputLayout address = (TextInputLayout) dialogSpInfo.findViewById(R.id.address);
        final TextView serviceType = (TextView) dialogSpInfo.findViewById(R.id.serviceType);
        final TextView provinceTxt = (TextView) dialogSpInfo.findViewById(R.id.provinceTxt);
        final TextInputLayout min = (TextInputLayout)dialogSpInfo.findViewById(R.id.mincost);
        final TextInputLayout max = (TextInputLayout)dialogSpInfo.findViewById(R.id.maxcost);

        final RatingBar infoRatingBar = dialogSpInfo.findViewById(R.id.info_rating);
        final TextView numberInfoRatingBar = dialogSpInfo.findViewById(R.id.number_info_rating);
        final RatingBar reviewRatingBar = dialogSpInfo.findViewById(R.id.review_rating);
        final TextInputLayout reviewInput = dialogSpInfo.findViewById(R.id.review_input);
        final TextView feedbackTitle = dialogSpInfo.findViewById(R.id.titleReviewList);
        final RecyclerView feedbackRecycleView = (RecyclerView) dialogSpInfo.findViewById(R.id.recycler_reviews);

        //fill value to controls
        name.getEditText().setText(stopPoint.getName());
        name.setEnabled(false);
        address.getEditText().setText(stopPoint.getAddress());
        address.setEnabled(false);
        min.getEditText().setText(stopPoint.getMinCost());
        min.setEnabled(false);
        max.getEditText().setText(stopPoint.getMaxCost());
        max.setEnabled(false);

        String[] servicestype = {getString(R.string.restaurant),getString(R.string.hotel),getString(R.string.reststation),getString(R.string.other)};
        final String[] provinceIds = getResources().getStringArray(R.array.proviceId);

        serviceType.setText(servicestype[stopPoint.getServiceTypeId()-1]);
        provinceTxt.setText(provinceIds[stopPoint.getProvinceId()]);

        Call<PointStats> callbackPointService = dataClient.getPointService(MyApplication.token, stopPoint.getId());
        callbackPointService.enqueue(new Callback<PointStats>() {
            @Override
            public void onResponse(Call<PointStats> call, Response<PointStats> response) {
                if(response.isSuccessful()) {
                    pointStats = response.body();
                    float averagePoint;
                    float sum = 0;
                    float count = 0;
                    for(int i =0; i<pointStats.getPointStats().size(); i++) {
                      sum += pointStats.getPointStats().get(i).getPoint() * pointStats.getPointStats().get(i).getTotal();
                      count += pointStats.getPointStats().get(i).getTotal();
                    }
                    averagePoint = (count == 0) ? 0: sum/count;
                    String averagePointStr = String.valueOf(averagePoint);
                    infoRatingBar.setRating(averagePoint);
                    if(averagePoint == 0) numberInfoRatingBar.setText(R.string.chua_co_danh_gia_nao);
                    else {
                        numberInfoRatingBar.setText(String.format("%.02f", averagePoint));
                    }
                }
            }
            @Override
            public void onFailure(Call<PointStats> call, Throwable t) {
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        feedbackRecycleView.setLayoutManager(layoutManager);
        feedbackAdapter = new FeedbackAdapter(feedbacksData, getApplicationContext());
        feedbackRecycleView.setAdapter(feedbackAdapter);
        feedbackRecycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastVisibleItemPosition() == feedbacksData.size() - 1) {
                        loadMoreFeedback();
                        isLoading = true;
                    }
                }
            }
        });
        Call<Feedbacks> callbackFeedbackServiceTotal = dataClient.getFeedbackService(
                MyApplication.token,
                stopPoint.getId(),
                1, MAX_SIZE
        );
        callbackFeedbackServiceTotal.enqueue(new Callback<Feedbacks>() {
            @Override
            public void onResponse(Call<Feedbacks> call, Response<Feedbacks> response) {
                if(response.isSuccessful()) {
                    feedbacks  = response.body();
                    totalsize = feedbacks.getFeedbacks().size();
                }
            }

            @Override
            public void onFailure(Call<Feedbacks> call, Throwable t) {

            }
        });

        final Call<Feedbacks> callbackFeedbackService = dataClient.getFeedbackService(
                MyApplication.token,
                stopPoint.getId(),
                1, PAGE_SIZE);
        callbackFeedbackService.enqueue(new Callback<Feedbacks>() {
            @Override
            public void onResponse(Call<Feedbacks> call, Response<Feedbacks> response) {
                if(response.isSuccessful()) {
                    feedbacks  = response.body();
                    if(feedbacks.getFeedbacks().size() == 0) feedbackTitle.setText(R.string.khong_co_luot_review_nao);
                    else {
                        feedbacksData.clear();
                        feedbacksData.addAll(feedbacks.getFeedbacks());
                        feedbackAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<Feedbacks> call, Throwable t) {
                Log.i("testne", "fail");
            }
        });
//        /
        // build dialog
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setView(dialogSpInfo);
        alert.setPositiveButton(R.string.gui_reviews, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int which) {
                // post api: getRating, string content
                String point = String.valueOf(Math.round(reviewRatingBar.getRating())) ;
                String feedback = reviewInput.getEditText().getText().toString();
                Call<MessageResponseRetrofit> callbackPostFeedback = dataClient.postFeedbackService(
                        MyApplication.token, stopPoint.getId(),
                        point,
                        feedback);

                callbackPostFeedback.enqueue(new Callback<MessageResponseRetrofit>() {
                    @Override
                    public void onResponse(Call<MessageResponseRetrofit> call, Response<MessageResponseRetrofit> response) {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onFailure(Call<MessageResponseRetrofit> call, Throwable t) {
                        Toast.makeText(getActivity(), "Lỗi hệ thống", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        alert.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        final AlertDialog dialog = alert.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);

        // handle send feedback
        reviewRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                // active button send review
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
            }
        });
        reviewInput.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                // active button send
                String content = reviewInput.getEditText().getText().toString();
                if(!content.isEmpty()) {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                }
            }
        });

    }

    private void loadMoreFeedback() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (feedbacksData.size() != totalsize) {
                    Call callLoadMore = RetrofitClient.getInstance().getApi().getFeedbackService(
                            MyApplication.token, stopPointId, loadedPage + 1, PAGE_SIZE
                    );
                    callLoadMore.enqueue(new Callback<Feedbacks>() {
                        @Override
                        public void onResponse(Call<Feedbacks> call, Response<Feedbacks> response) {
                            if (response.isSuccessful()) {
                                feedbacksData.addAll(response.body().getFeedbacks());
                                feedbackAdapter.notifyDataSetChanged();
                                loadedPage++;
                            }
                            isLoading = false;
                        }
                        @Override
                        public void onFailure(Call<Feedbacks> call, Throwable t) {
                            isLoading = false;
                        }
                    });
                }
            }
        }, 500);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(getContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                        locationManager.requestLocationUpdates(provider, 400, 1, this);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

        }
    }

    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        dataClient = APIUtils.getData();

        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            locationManager =(LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_LOW);
            provider = locationManager.getBestProvider(criteria,true);
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mMap = googleMap;
                    if (checkLocationPermission())
                    mMap.setMyLocationEnabled(true);
                }
            });
            getChildFragmentManager().beginTransaction().add(R.id.map, mapFragment).commit();
        }

        final String[] provinceIds = getResources().getStringArray(R.array.proviceId);
        provinceSpinner = rootView.findViewById(R.id.spinner_province);
        ArrayAdapter<String> provinceSpinnerAdapter = new ArrayAdapter<String>(getContext()
                ,android.R.layout.simple_spinner_dropdown_item, provinceIds);
        provinceSpinner.setAdapter(provinceSpinnerAdapter);

        provinceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                provinceId = parent.getSelectedItemPosition() + 1;
                provinceName = provinceIds[provinceId - 1];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Lấy value search
        searchBtn = rootView.findViewById(R.id.search_btn);
        searchInput= rootView.findViewById(R.id.search_keyword);

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // validate: require searchKey với provinceName
                searchKey = searchInput.getText().toString();

                Call<StopPoints> callback = dataClient.getSearchStopPoint(MyApplication.token, searchKey, provinceId, provinceName, 1, PAGE_SIZE);
                callback.enqueue(new Callback<StopPoints>() {
                    @Override
                    public void onResponse(Call<StopPoints> call, Response<StopPoints> response) {
                        if(response.isSuccessful()) {
                            stopPoints = response.body();
                            createStopPointsOnMap(stopPoints.getStopPoints());
                        }
                    }

                    @Override
                    public void onFailure(Call<StopPoints> call, Throwable t) {

                    }
                });
            }
        });
//
//

        return rootView;
    }
}
