package com.ygaps.travelapp.TourActivities.ui.main;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.ygaps.travelapp.MyApplication;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.data.model.MessageResponseRetrofit;
import com.ygaps.travelapp.data.model.PointStat;
import com.ygaps.travelapp.data.model.PointStats;
import com.ygaps.travelapp.data.model.Reviewer;
import com.ygaps.travelapp.data.model.Reviewers;
import com.ygaps.travelapp.data.model.StopPoints;
import com.ygaps.travelapp.data.model.TourInfo;
import com.ygaps.travelapp.data.model.Tours;
import com.ygaps.travelapp.main.ReviewAdapter;
import com.ygaps.travelapp.main.ToursAdapter;
import com.ygaps.travelapp.retrofit.APIUtils;
import com.ygaps.travelapp.retrofit.DataClient;
import com.ygaps.travelapp.retrofit.RetrofitClient;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ReviewTourFragment extends Fragment {

  private RatingBar starRating;
  private TextView numberRating;
  private RecyclerView recyclerView;
  private TextInputLayout inputReview;
  private Button sendBtn;
  private RatingBar sendRating;
  private TextView nameTourView;
  private TextView dateTourView;
  private TextView costTourView;
  private TextView adultsTourView;
  private TextView childsTourView;

  private PageViewModel pageViewModel;

  ArrayList<Reviewer> listReviews = new ArrayList<>();
  private Reviewers reviewers;
  private PointStats pointStats;
  private int tourId;
  private String nameTour;
  private String startDateTour;
  private String endDateTour;
  private String minCostTour;
  private String maxCostTour;
  private int childsTour;
  private int adultsTour;
  private String token;

  private boolean isLoading = false;
  private int loadedPage = 1;
  private int totalsize = 0;
  private final int PAGE_SIZE = 10;
  private final int MAX_SIZE = 100;
  ReviewAdapter reviewAdapter;
  public ReviewTourFragment() {
    // Required empty public constructor
  }

  public static ReviewTourFragment newInstance() {
    ReviewTourFragment fragment = new ReviewTourFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    pageViewModel = ViewModelProviders.of(getActivity()).get(PageViewModel.class);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {

    View root = inflater.inflate(R.layout.review_tour, container, false);

    nameTourView = root.findViewById(R.id.name_tour);
    dateTourView = root.findViewById(R.id.date_tour);
    costTourView = root.findViewById(R.id.cost_tour);
    adultsTourView = root.findViewById(R.id.adults_tour);
    childsTourView = root.findViewById(R.id.childs_tour);
    recyclerView = root.findViewById(R.id.recycler_reviews);
    inputReview = root.findViewById(R.id.reviewInput);
    sendBtn = root.findViewById(R.id.send_btn);
    starRating = root.findViewById(R.id.star_rating);
    numberRating = root.findViewById(R.id.number_rating);
    sendRating = root.findViewById(R.id.send_rating);

    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
    recyclerView.setHasFixedSize(true);
    recyclerView.setLayoutManager(linearLayoutManager);
    recyclerView.setNestedScrollingEnabled(false);
    reviewAdapter = new ReviewAdapter(listReviews, getApplicationContext());
    recyclerView.setAdapter(reviewAdapter);
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
      }
      @Override
      public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (!isLoading) {
          if (linearLayoutManager != null && linearLayoutManager.findLastVisibleItemPosition() == listReviews.size() - 1) {
            loadMoreReview();
            isLoading = true;
          }
        }
      }
    });

    SharedPreferences sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences("token", Context.MODE_PRIVATE);
    token = sharedPreferences.getString("token", "");

    final DataClient dataClient = APIUtils.getData();
    pageViewModel.getObserveable().observe(getViewLifecycleOwner(), new Observer<TourInfo>() {
      @Override
      public void onChanged(TourInfo tourInfo) {
        if(tourInfo != null) {
          tourId = tourInfo.getId();
          nameTour = tourInfo.getName();
          startDateTour = tourInfo.getStartDate();
          endDateTour = tourInfo.getEndDate();
          minCostTour = tourInfo.getMinCost();
          maxCostTour = tourInfo.getMaxCost();
          childsTour = tourInfo.getChilds();
          adultsTour = tourInfo.getAdults();


          if (nameTour == null) {
            nameTourView.setText(R.string.mac_dinh);
          }
          else {
            nameTourView.setText(nameTour);
          }
          if (startDateTour == null || endDateTour == null) {
            dateTourView.setText(R.string.mac_dinh_mac_dinh);
          }
          else {
            @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            long milisecStart = Long.parseLong(startDateTour);
            Date dateStart = new Date(milisecStart);
            dateFormat.format(dateStart);

            long milisecEnd = Long.parseLong(endDateTour);
            Date dateEnd = new Date(milisecEnd);
            dateFormat.format(dateEnd);
            dateTourView.setText(dateFormat.format(dateStart) + " đến " + dateFormat.format(dateEnd));
          }
          if (minCostTour == null || maxCostTour == null) {
            costTourView.setText(R.string.mac_dinh_mac_dinh);
          }
          else {
            costTourView.setText(minCostTour + " - " + maxCostTour);
          }

          childsTourView.setText(String.valueOf(childsTour));
          adultsTourView.setText(String.valueOf(adultsTour));


          Call<PointStats> callback = dataClient.getReviewStar(token, tourId);
          callback.enqueue(new Callback<PointStats>() {
            @Override
            public void onResponse(Call<PointStats> call, Response<PointStats> response) {
              if(response.isSuccessful()) {
                pointStats = response.body();
                createPointStatAverage(pointStats.getPointStats());
              }
            }
            @Override
            public void onFailure(Call<PointStats> call, Throwable t) {

            }
          });

          Call<Reviewers> callbackListReviewTotal = dataClient.getReviewList(token, tourId, 1, MAX_SIZE);
          callbackListReviewTotal.enqueue(new Callback<Reviewers>() {
            @Override
            public void onResponse(Call<Reviewers> call, Response<Reviewers> response) {
              if(response.isSuccessful()) {
                reviewers = response.body();
                totalsize = reviewers.getReviews().size();
              }
            }
            @Override
            public void onFailure(Call<Reviewers> call, Throwable t) {
            }
          });

          Call<Reviewers> callbackListReview = dataClient.getReviewList(token, tourId, 1, PAGE_SIZE);
          callbackListReview.enqueue(new Callback<Reviewers>() {
            @Override
            public void onResponse(Call<Reviewers> call, Response<Reviewers> response) {
              if(response.isSuccessful()) {
                reviewers = response.body();
                listReviews.clear();
                listReviews.addAll(reviewers.getReviews());
                reviewAdapter.notifyDataSetChanged();
              }
            }
            @Override
            public void onFailure(Call<Reviewers> call, Throwable t) {

            }
          });
        }
      }
    });
    // handle events to post api
    sendBtn.setEnabled(false);
    sendRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
      @Override
      public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        sendBtn.setEnabled(true);
      }
    });
    inputReview.getEditText().addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }
      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        String content = inputReview.getEditText().getText().toString();
        if(!content.isEmpty()) {
          sendBtn.setEnabled(true);
        }
      }
    });

    sendBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String content = inputReview.getEditText().getText().toString();
        String point = String.valueOf(Math.round(sendRating.getRating()));
        retrofit2.Call callbackPostReview = RetrofitClient
                .getInstance()
                .getApi()
                .postReview(token, tourId, point, content);
        callbackPostReview.enqueue(new Callback<MessageResponseRetrofit>() {
          @Override
          public void onResponse(Call<MessageResponseRetrofit> call, Response<MessageResponseRetrofit> response) {
            inputReview.getEditText().setText("");
            Toast.makeText(getActivity(), response.body().getMessage(),
                    Toast.LENGTH_LONG).show();
          }
          @Override
          public void onFailure(Call<MessageResponseRetrofit> call, Throwable t) {

          }
        });
      }
    });

    // Inflate the layout for this fragment
    return root;
  }

  private void loadMoreReview() {
    Handler handler = new Handler();
    handler.postDelayed(new Runnable() {
      @Override
      public void run() {
        if (listReviews.size() != totalsize) {
          Call callLoadMore = RetrofitClient.getInstance().getApi().getReviewList(token, tourId, loadedPage + 1, PAGE_SIZE);
          callLoadMore.enqueue(new Callback<Reviewers>() {
            @Override
            public void onResponse(Call<Reviewers> call, Response<Reviewers> response) {
              if (response.isSuccessful()) {
                listReviews.addAll(response.body().getReviews());
                reviewAdapter.notifyDataSetChanged();
                loadedPage++;
              }
              isLoading = false;
            }
            @Override
            public void onFailure(Call<Reviewers> call, Throwable t) {
              isLoading = false;
            }
          });
        }
      }
    }, 500);
  }

  public void createPointStatAverage(List<PointStat> pointStats) {
    float averagePoint;
    float sum = 0;
    float count = 0;
    for(int i =0; i<pointStats.size(); i++) {
      sum += pointStats.get(i).getPoint() * pointStats.get(i).getTotal();
      count += pointStats.get(i).getTotal();
    }

    averagePoint = (count == 0) ? 0: sum/count;
    String averagePointStr = String.valueOf(averagePoint);
    starRating.setRating(averagePoint);
    if(averagePoint == 0) numberRating.setText(R.string.chua_co_danh_gia_nao);
    else {
      numberRating.setText(averagePointStr);
    }
  }

}
