package com.ygaps.travelapp;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import static com.facebook.share.internal.DeviceShareDialogFragment.TAG;

public class MyApplication extends Application {
  static public String token;
  static public String FCMtoken;
  static public String deviceId;
  static public String userId;
  public static final String YES_ACTION = "YES_ACTION";
  public static final String NO_ACTION = "NO_ACTION";
  public static final String STOP_TRACKING_ACTION = "STOP_TRACKING_ACTION";
  public static BroadcastReceiver broadcastReceiver;
  @Override
  public void onCreate() {
    broadcastReceiver = new ReceiveNotiButtonAction();
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction(YES_ACTION);
    intentFilter.addAction(NO_ACTION);
    intentFilter.addAction(STOP_TRACKING_ACTION);
    registerReceiver(broadcastReceiver,intentFilter);
    deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(),
            Settings.Secure.ANDROID_ID);
    SharedPreferences sharedPreferences = getSharedPreferences("token", MODE_PRIVATE);
    token = sharedPreferences.getString("token", "");
    userId = sharedPreferences.getString("userId", "");
    super.onCreate();
  }

  @Override
  public void onTerminate() {
    unregisterReceiver(broadcastReceiver);
    super.onTerminate();
  }

  public MyApplication() {
    super();
  }
}
