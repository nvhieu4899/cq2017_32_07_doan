package com.ygaps.travelapp.Nav_Fragment;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.ygaps.travelapp.MyApplication;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.data.model.ListInvitation;
import com.ygaps.travelapp.data.model.Tours;
import com.ygaps.travelapp.retrofit.RetrofitClient;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoticationFragment extends Fragment {

  private RecyclerView recyclerView;
  private InvitationAdapter invitationAdapter;
  private ArrayList<ListInvitation.Invitation> invitations = new ArrayList<>();
  private int total;
  private boolean isLoading = false;
  private int loadedPage;
  private final int PAGE_SIZE = 10;

  public NoticationFragment() {
    // Required empty public constructor
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment

    View view = inflater.inflate(R.layout.fragment_notication, container, false);
    recyclerView = view.findViewById(R.id.recycler);
    recyclerView.setHasFixedSize(true);
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
    recyclerView.setLayoutManager(linearLayoutManager);

    invitationAdapter = new InvitationAdapter();
    recyclerView.setAdapter(invitationAdapter);

    Call call = RetrofitClient.getInstance().getApi().getListInvitation(MyApplication.token,1,String.valueOf(PAGE_SIZE));
    call.enqueue(new Callback<ListInvitation>(){
      @Override
      public void onResponse(Call<ListInvitation> call, Response<ListInvitation> response) {
        total=response.body().getTotal();
        loadedPage =1;
        invitations.clear();
        invitations.addAll(response.body().getTours());
        invitationAdapter.notifyDataSetChanged();
      }

      @Override
      public void onFailure(Call<ListInvitation> call, Throwable t) {

      }
    });


    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
      }

      @Override
      public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        LinearLayoutManager linearLayoutManager1 = (LinearLayoutManager) recyclerView.getLayoutManager();

        if (!isLoading)
        {
          if (linearLayoutManager1!=null&&linearLayoutManager1.findLastVisibleItemPosition()==invitations.size()-1)
          {
            loadMoreInvitations();
            isLoading = true;
          }
        }
      }
    });
    return view;
  }

  public class InvitationAdapter extends RecyclerView.Adapter<InvitationViewHolder> {
    @NonNull
    @Override
    public InvitationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      LayoutInflater inflater = LayoutInflater.from(parent.getContext());
      View view = inflater.inflate(R.layout.recycler_item_invitations, parent, false);
      return new InvitationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final InvitationViewHolder holder, int position) {
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");


      final ListInvitation.Invitation invitation = invitations.get(position);

      String startDate;
      String endDate;
      String minCost;
      String maxCost;
      if (invitation.getStartDate() == null) startDate = getString(R.string.str_unknown);
      else {
        startDate = simpleDateFormat.format(new Date(Long.valueOf(invitation.getStartDate())));
      }
      if (invitation.getEndDate() == null) endDate = getString(R.string.str_unknown);
      else {
        endDate = simpleDateFormat.format(new Date(Long.valueOf(invitation.getStartDate())));
      }
      holder.date.setText(startDate + " - " + endDate);

      holder.hostName.setText(invitation.getHostName());
      if (invitation.getAvatar() == null) {
        holder.avatarTour.setImageResource(R.drawable.img_default);
      } else
        Picasso.get().load(invitation.getAvatar()).error(R.drawable.img_default).into(holder.avatarTour);
      if (invitation.getMinCost() == null)
        minCost = getString(R.string.str_unknown);
      else minCost = invitation.getMinCost();
      if (invitation.getMaxCost() == null)
        maxCost = getString(R.string.str_unknown);
      else maxCost = invitation.getMaxCost();

      holder.name.setText(invitation.getName());

      holder.cost.setText(minCost + " - " + maxCost);


      holder.accept.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          holder.accept.setEnabled(false);
          holder.deny.setEnabled(false);
          Call call = RetrofitClient.getInstance().getApi().responseToAnInvitation(MyApplication.token, String.valueOf(invitation.getId()), true);
          call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
              if (response.isSuccessful()) {
                Toast.makeText(getContext(), R.string.accept_invite_message, Toast.LENGTH_LONG).show();
              }
              else {
                holder.accept.setEnabled(false);
                holder.deny.setEnabled(false);
              }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
              holder.accept.setEnabled(true);
              holder.deny.setEnabled(true);
            }
          });

        }
      });
      holder.deny.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          holder.accept.setEnabled(false);
          holder.deny.setEnabled(false);
          Call call = RetrofitClient.getInstance().getApi().responseToAnInvitation(MyApplication.token, String.valueOf(invitation.getId()), false);
          call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
              if (response.isSuccessful()) {
                Toast.makeText(getContext(), R.string.deny_invite_message, Toast.LENGTH_LONG).show();
              }
              else {
                holder.accept.setEnabled(false);
                holder.deny.setEnabled(false);
              }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
              holder.accept.setEnabled(true);
              holder.deny.setEnabled(true);
            }
          });
        }
      });
    }
    @Override
    public int getItemCount() {
      return invitations.size();
    }
  }


  public class InvitationViewHolder extends RecyclerView.ViewHolder implements OnClickListener {
    private ImageView avatarTour;
    private TextView name;
    private TextView date;
    private TextView cost;
    private TextView hostName;
    private Button accept;
    private Button deny;

    public InvitationViewHolder(@NonNull View itemView) {
      super(itemView);
      avatarTour = (ImageView) itemView.findViewById(R.id.avatar_tour);
      name = (TextView) itemView.findViewById(R.id.tourName);
      date = itemView.findViewById(R.id.date);
      cost = itemView.findViewById(R.id.cost);
      hostName = itemView.findViewById(R.id.hostName);
      accept = (Button)itemView.findViewById(R.id.accept);
      deny = (Button) itemView.findViewById(R.id.deny);
    }

    @Override
    public void onClick(View v) {
      Toast.makeText(getContext(), "clicked", Toast.LENGTH_SHORT).show();
      Log.i("testne", "clicked !" + name);
    }
  }
  private void loadMoreInvitations()
  {
    Handler handler = new Handler();
    handler.postDelayed(new Runnable() {
      @Override
      public void run() {
        if (invitations.size() != total) {
          Call call = RetrofitClient.getInstance().getApi().getListInvitation(MyApplication.token,loadedPage+1,String.valueOf(PAGE_SIZE));
          call.enqueue(new Callback<ListInvitation>(){
            @Override
            public void onResponse(Call<ListInvitation> call, Response<ListInvitation> response) {
              total=response.body().getTotal();
              loadedPage ++;
              invitations.addAll(response.body().getTours());
              invitationAdapter.notifyDataSetChanged();
              isLoading = false;
            }
            @Override
            public void onFailure(Call<ListInvitation> call, Throwable t) {
              isLoading= false;
            }
          });
        }
      }
    }, 500);
  }
}
