package com.ygaps.travelapp.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.TourActivities.ManageTourHistory;
import com.ygaps.travelapp.data.model.Tour;

import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ToursAdapter extends RecyclerView.Adapter<ToursAdapter.ViewHolder> {
    ArrayList<Tour> tours;
    Context context;

    public ToursAdapter(ArrayList<Tour> tours, Context context) {
        this.tours = tours;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView = layoutInflater.inflate(R.layout.recycler_item_tours, parent, false);
        return new ViewHolder(itemView);
    }

    public static Drawable LoadImageFromWebOperations(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }
    public static String FormatDate(String milisecondString) {
        if (milisecondString==null) return "chưa biết";
        long milisec = Long.parseLong(milisecondString);
        Date date = new Date(milisec);
        @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("dd - MM - yyyy");
        return dateFormat.format(date);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        // Xử lí định dạng date
        String startDayFormated = FormatDate(tours.get(position).getStartDate());
        String endDayFormated = FormatDate(tours.get(position).getEndDate());


        if(tours.get(position).getAvatar() == null) {
            holder.imgTour.setImageResource(R.drawable.img_default);
        }
        else {
            Picasso.get().load(tours.get(position).getAvatar()).error(R.drawable.img_default).into(holder.imgTour);
        }

        holder.titleTour.setText(tours.get(position).getName());
        holder.startDate.setText(startDayFormated);
        holder.endDate.setText(endDayFormated);
        holder.maxCost.setText(tours.get(position).getMaxCost());
        holder.minCost.setText(tours.get(position).getMinCost());

    }

    @Override
    public int getItemCount() {
        return tours.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgTour;
        TextView titleTour;
        TextView startDate;
        TextView endDate;
        TextView maxCost;
        TextView minCost;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgTour = (ImageView) itemView.findViewById(R.id.img_tour_recycler);
            titleTour = (TextView) itemView.findViewById(R.id.title_tour_recycler);
            startDate = (TextView) itemView.findViewById(R.id.startDate_tour_recycler);
            endDate = (TextView) itemView.findViewById(R.id.endDate_tour_recycler);
            maxCost = (TextView) itemView.findViewById(R.id.maxCost_tour_recycler);
            minCost = (TextView) itemView.findViewById(R.id.minCost_tour_recycler);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), ManageTourHistory.class);
            intent.putExtra("tourId",tours.get(getAdapterPosition()).getId());
            intent.putExtra("history",0);
            intent.putExtra("hostid", tours.get(getAdapterPosition()).getHostId());
            v.getContext().startActivity(intent);
        }
    }
}
