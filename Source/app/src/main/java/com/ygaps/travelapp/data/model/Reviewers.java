package com.ygaps.travelapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Reviewers {

    @SerializedName("reviewList")
    @Expose
    private List<Reviewer> reviews;
    public List<Reviewer> getReviews() {
        return reviews;
    }

    public void setReviews(List<Reviewer> reviews) {
        this.reviews = reviews;
    }
}
