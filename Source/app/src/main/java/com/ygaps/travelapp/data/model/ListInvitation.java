package com.ygaps.travelapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ListInvitation {
  @SerializedName("total")
  @Expose
  private int total;
  @SerializedName("tours")
  @Expose
  private ArrayList<Invitation> tours;

  public int getTotal() {
    return total;
  }

  public void setTotal(int total) {
    this.total = total;
  }

  public ArrayList<Invitation> getTours() {
    return tours;
  }

  public void setTours(ArrayList<Invitation> tours) {
    this.tours = tours;
  }

  public class Invitation{
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("hostId")
    @Expose
    private int hostId;
    @SerializedName("hostName")
    @Expose
    private String hostName;
    @SerializedName("hostPhone")
    @Expose
    private String hostPhone;
    @SerializedName("hostEmail")
    @Expose
    private String hostEmail;
    @SerializedName("hostAvatar")
    @Expose
    private String hostAvatar;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("minCost")
    @Expose
    private String minCost;
    @SerializedName("maxCost")
    @Expose
    private String maxCost;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("endDate")
    @Expose
    private String endDate;
    @SerializedName("adults")
    @Expose
    private int adults;
    @SerializedName("childs")
    @Expose
    private int childs;
    @SerializedName("isPrivate")
    @Expose
    private boolean isPrivate;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("createdOn")
    @Expose
    private String createOn;

    public int getId() {
      return id;
    }

    public void setId(int id) {
      this.id = id;
    }

    public int getStatus() {
      return status;
    }

    public void setStatus(int status) {
      this.status = status;
    }

    public int getHostId() {
      return hostId;
    }

    public void setHostId(int hostId) {
      this.hostId = hostId;
    }

    public String getHostName() {
      return hostName;
    }

    public void setHostName(String hostName) {
      this.hostName = hostName;
    }

    public String getHostPhone() {
      return hostPhone;
    }

    public void setHostPhone(String hostPhone) {
      this.hostPhone = hostPhone;
    }

    public String getHostEmail() {
      return hostEmail;
    }

    public void setHostEmail(String hostEmail) {
      this.hostEmail = hostEmail;
    }

    public String getHostAvatar() {
      return hostAvatar;
    }

    public void setHostAvatar(String hostAvatar) {
      this.hostAvatar = hostAvatar;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getMinCost() {
      return minCost;
    }

    public void setMinCost(String minCost) {
      this.minCost = minCost;
    }

    public String getMaxCost() {
      return maxCost;
    }

    public void setMaxCost(String maxCost) {
      this.maxCost = maxCost;
    }

    public String getStartDate() {
      return startDate;
    }

    public void setStartDate(String startDate) {
      this.startDate = startDate;
    }

    public String getEndDate() {
      return endDate;
    }

    public void setEndDate(String endDate) {
      this.endDate = endDate;
    }

    public int getAdults() {
      return adults;
    }

    public void setAdults(int adults) {
      this.adults = adults;
    }

    public int getChilds() {
      return childs;
    }

    public void setChilds(int childs) {
      this.childs = childs;
    }

    public boolean isPrivate() {
      return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
      isPrivate = aPrivate;
    }

    public String getAvatar() {
      return avatar;
    }

    public void setAvatar(String avatar) {
      this.avatar = avatar;
    }

    public String getCreateOn() {
      return createOn;
    }

    public void setCreateOn(String createOn) {
      this.createOn = createOn;
    }
  }
}
