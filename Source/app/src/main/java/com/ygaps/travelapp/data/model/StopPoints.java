package com.ygaps.travelapp.data.model;

import java.util.ArrayList;

public class StopPoints {
    private ArrayList<StopPoint> stopPoints;

    public ArrayList<StopPoint> getStopPoints() {
        return stopPoints;
    }

    public void setStopPoints(ArrayList<StopPoint> stopPoints) {
        this.stopPoints = stopPoints;
    }

    public StopPoints(ArrayList<StopPoint> stopPoints) {
        this.stopPoints = stopPoints;
    }
}
