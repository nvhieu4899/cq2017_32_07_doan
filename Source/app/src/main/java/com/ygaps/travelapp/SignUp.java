package com.ygaps.travelapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ygaps.travelapp.retrofit.RetrofitClient;
import com.ygaps.travelapp.ui.login.LoginActivity;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUp extends AppCompatActivity {
    private Button btn_reg;
    private String dob="";
    private TextInputLayout input_email;
    private TextInputLayout input_name;
    private TextInputLayout input_pass;
    private TextInputLayout input_pass_rep;
    private TextInputLayout input_address;
    private TextInputLayout input_phone;
    private TextView dateView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        btn_reg=(Button) findViewById(R.id.signup_btn);
        input_email = (TextInputLayout)findViewById(R.id.signup_email);
        input_email.setFocusedByDefault(true);
        input_name = (TextInputLayout)findViewById(R.id.signup_name);
        input_pass = (TextInputLayout)findViewById(R.id.signup_password);
        input_pass_rep = (TextInputLayout)findViewById(R.id.signup_password_rep);
        input_phone = (TextInputLayout)findViewById(R.id.signup_phone);
        input_address = (TextInputLayout)findViewById(R.id.signup_address);
        dateView = (TextView)findViewById(R.id.signup_dob);

        RadioGroup radioGroup = (RadioGroup)findViewById(R.id.radio_gender_group);
        radioGroup.check(R.id.signup_gender_male);
        dateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePick();
            }
        });


        btn_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_reg.setEnabled(false);
                if (!validatePhone()|!validatePassword()|!validateEmail())
                {
                    btn_reg.setEnabled(true);
                    return;
                }

                String email = input_email.getEditText().getText().toString();
                String pass = input_pass.getEditText().getText().toString();
                String phone  = input_phone.getEditText().getText().toString();
                String name = input_name.getEditText().getText().toString();
                String address = input_address.getEditText().getText().toString();
                int gender;
                RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radio_gender_group);
                if (radioGroup.getCheckedRadioButtonId()==R.id.signup_gender_male) gender=1;
                else gender = 0;
                retrofit2.Call call = RetrofitClient
                        .getInstance()
                        .getApi()
                        .createUser(pass,name,email,phone,address,dob,gender);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            String s = null;
                            if (response.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), getString(R.string.str_success_register), Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                                finish();
                            try
                            {
                                s = response.body().string();

                            } catch (IOException e)
                            {
                                e.printStackTrace();
                            }

                            } else
                            {
                                try
                                {
                                    s = response.errorBody().string();
                                    if (s!=null)
                                    {
                                        try {
                                            JSONObject jsonObject = new JSONObject(s);
                                            JSONArray messages = jsonObject.getJSONArray("message");
                                            for (int i = 0;i<messages.length();i++)
                                            {
                                                if (messages.getJSONObject(i).getString("msg").equals("Email already registered"))
                                                {
                                                    Toast.makeText(getApplicationContext(), getString(R.string.str_alreadyUsedEmail), Toast.LENGTH_LONG).show();
                                                    input_email.requestFocus();
                                                    input_email.setError(getString(R.string.str_alreadyUsedEmail));
                                                }
                                                else if (messages.getJSONObject(i).getString("param").equals("phone"))
                                                {
                                                    Toast.makeText(getApplicationContext(), getString(R.string.str_alreadyUsedPhonenumber), Toast.LENGTH_LONG).show();
                                                    input_phone.requestFocus();
                                                    input_phone.setError(getString(R.string.str_alreadyUsedPhonenumber));
                                                }
                                            }
                                        }
                                        catch (JSONException e)
                                        {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                                catch (IOException e)
                                {
                                    e.printStackTrace();
                                }
                            }
                    }


                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }

                });
                btn_reg.setEnabled(true);
            }
        });
    }
    private void datePick()
    {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year, month, dayOfMonth);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
                SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd-MM-YYYY");
                dateView.setText(simpleDateFormat2.format(calendar.getTime()));
                dob = simpleDateFormat.format(calendar.getTime());
            }
        },year,month,day);
        datePickerDialog.show();
    }

    private boolean validateEmail()
    {
        String emailInput = input_email.getEditText().getText().toString().trim();
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";

        if (emailInput.isEmpty())
        {
            input_email.setError(getString(R.string.str_required));
            input_email.requestFocus();
            return false;
        }
        else
        {
            input_email.setError(null);
        }
        if (!emailInput.matches(regex))
        {
            input_email.setError(getString(R.string.str_notvalidated_email));
            input_email.requestFocus();

            return false;
        }
        else
        {
            input_email.setError(null);
        }

        return true;
    }
    private boolean validatePassword()
    {
        String passwordInput = input_pass.getEditText().getText().toString().trim();
        String passwordInput_rep = input_pass_rep.getEditText().getText().toString().trim();

        if (passwordInput.isEmpty())
        {
            input_pass.setError(getString(R.string.str_required));
            input_email.requestFocus();

            return false;
        }
        else
        {
            input_pass.setError(null);
        }
        if (passwordInput_rep.isEmpty())
        {
            input_pass_rep.setError(getString(R.string.str_required));
            input_email.requestFocus();

            return false;
        }
        else
        {
            input_pass_rep.setError(null);
        }
        if (passwordInput.compareTo(passwordInput_rep)!=0)
        {
            input_pass_rep.setError(getString(R.string.str_samePassRequired));
            input_email.requestFocus();

            return false;
        }
        else
        {
            input_pass_rep.setError(null);
        }
        return true;
    }
    private boolean validatePhone()
    {
        String phoneInput = input_phone.getEditText().getText().toString().trim();
        String regex = "^[+]?[0-9]{10,13}$";
        if (phoneInput.isEmpty()|!phoneInput.matches(regex))
        {
            input_phone.setError(getString(R.string.str_phone_unvalid));
            input_email.requestFocus();
            return false;
        }
        else
        {
            input_phone.setError(null);
            return true;
        }

    }


}
