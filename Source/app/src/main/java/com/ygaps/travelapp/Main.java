package com.ygaps.travelapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.ygaps.travelapp.Nav_Fragment.HistoryFragment;
import com.ygaps.travelapp.Nav_Fragment.ListFragment;
import com.ygaps.travelapp.Nav_Fragment.MapFragment;
import com.ygaps.travelapp.Nav_Fragment.NoticationFragment;
import com.ygaps.travelapp.Nav_Fragment.SettingFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ygaps.travelapp.retrofit.RetrofitClient;

import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.share.internal.DeviceShareDialogFragment.TAG;

public class Main extends AppCompatActivity {

  private BottomNavigationView bottomNavigationView;
  private ListFragment listFragment = new ListFragment();
  private HistoryFragment historyFragment = new HistoryFragment();
  private MapFragment mapFragment = new MapFragment();
  private NoticationFragment noticationFragment = new NoticationFragment();
  private SettingFragment settingFragment = new SettingFragment();
  private ViewPager viewPager;
  private MenuItem prevMenuItem;
  @Override
  protected void onCreate(final Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    FirebaseInstanceId.getInstance().getInstanceId()
            .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
              @Override
              public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                  Log.w(TAG, "getInstanceId failed", task.getException());
                  return;
                }
                String token = task.getResult().getToken();
                Call call = RetrofitClient.getInstance().getApi().registerFirebaseToken(MyApplication.token, token, MyApplication.deviceId, 1, "1.0");
                call.enqueue(new Callback() {
                  @Override
                  public void onResponse(Call call, Response response) {
                    if (response.isSuccessful()) {
                      Log.d(TAG, response.body().toString());
                    } else Log.d(TAG, response.errorBody().toString());
                  }

                  @Override
                  public void onFailure(Call call, Throwable t) {

                  }
                });
                MyApplication.FCMtoken = token;
              }
            });

    IntentFilter filter = new IntentFilter();
    filter.addAction(MyApplication.YES_ACTION);
    filter.addAction(MyApplication.NO_ACTION);

    bottomNavigationView = findViewById(R.id.bottom_nav);


    viewPager = (ViewPager) findViewById(R.id.viewpager);
    ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,new ArrayList<Fragment>());
    viewPagerAdapter.addFragment(listFragment);
    viewPagerAdapter.addFragment(historyFragment);
    viewPagerAdapter.addFragment(mapFragment);
    viewPagerAdapter.addFragment(noticationFragment);
    viewPagerAdapter.addFragment(settingFragment);
    viewPager.setAdapter(viewPagerAdapter);

    viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

      }

      @Override
      public void onPageSelected(int position) {
        if (prevMenuItem != null) {
          prevMenuItem.setChecked(false);
        } else {
          bottomNavigationView.getMenu().getItem(0).setChecked(false);
        }
        bottomNavigationView.getMenu().getItem(position).setChecked(true);
        prevMenuItem = bottomNavigationView.getMenu().getItem(position);
      }

      @Override
      public void onPageScrollStateChanged(int state) {

      }
    });


    bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
      @Override
      public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id == R.id.list) {
          viewPager.setCurrentItem(0);
          return true;
        } else if (id == R.id.history) {
          viewPager.setCurrentItem(1);

          return true;
        } else if (id == R.id.map) {
          viewPager.setCurrentItem(2);

          return true;
        } else if (id == R.id.notication) {
          viewPager.setCurrentItem(3);

          return true;
        } else if (id == R.id.setting) {
          viewPager.setCurrentItem(4);

          return true;
        }
        return false;
      }
    });
    Intent intent = getIntent();
    int page = intent.getIntExtra("page", 0);
    bottomNavigationView.setSelectedItemId(page);
  }
  @Override
  public void onBackPressed() {
    setResult(100);
    super.onBackPressed();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
  }
}
