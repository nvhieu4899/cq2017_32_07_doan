package com.ygaps.travelapp.retrofit.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.data.model.Coordinate;

public class GetOneSuggestSP {
    @SerializedName("hasOneCoordinate")
    @Expose
    private boolean hasOneCoordinate = true;
    @SerializedName("coordList")
    @Expose
    private Coordinate coordinate;

    public GetOneSuggestSP(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public boolean isHasOneCoordinate() {
        return hasOneCoordinate;
    }

    public void setHasOneCoordinate(boolean hasOneCoordinate) {
        this.hasOneCoordinate = hasOneCoordinate;
    }
}
