package com.ygaps.travelapp.data.model;

import java.util.ArrayList;

public class Notifications {
    ArrayList<Notification> notifications;

    public ArrayList<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(ArrayList<Notification> notifications) {
        this.notifications = notifications;
    }
}
