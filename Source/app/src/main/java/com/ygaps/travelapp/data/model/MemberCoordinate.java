package com.ygaps.travelapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MemberCoordinate {
  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("lat")
  @Expose
  private String lat;
  @SerializedName("long")
  @Expose
  private String _long;

  public MemberCoordinate(String id, String lat, String _long) {
    this.id = id;
    this.lat = lat;
    this._long = _long;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getLat() {
    return lat;
  }

  public void setLat(String lat) {
    this.lat = lat;
  }

  public String get_long() {
    return _long;
  }

  public void set_long(String _long) {
    this._long = _long;
  }
}
