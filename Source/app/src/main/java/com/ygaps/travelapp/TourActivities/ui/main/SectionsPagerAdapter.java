package com.ygaps.travelapp.TourActivities.ui.main;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ygaps.travelapp.R;


/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

  @StringRes
  private static int[] TAB_TITLES;
  private final Context mContext;

  public SectionsPagerAdapter(Context context, FragmentManager fm, int his) {
    super(fm);
    mContext = context;
    if (his == 1) TAB_TITLES = new int[]{R.string.tours, R.string.member, R.string.chat,R.string.comments,R.string.reviews};
    else TAB_TITLES = new int[]{R.string.tours, R.string.member,R.string.comments,R.string.reviews};
  }

  @Override
  public Fragment getItem(int position) {
    int pos = position;
    if (TAB_TITLES.length == 4 && position >= 2) pos+=1;
    switch (pos) {
      case 0:
        return TourInfoFragment.newInstance();
      case 1:
        return MemberFragment.newInstance();
      case 2:
        return ChatFragment.newInstance();
      case 3:
        return CommentTourFragment.newInstance();
      case 4:
        return ReviewTourFragment.newInstance();
      default:
        return null;
    }
  }

  @Nullable
  @Override
  public CharSequence getPageTitle(int position) {
    return mContext.getResources().getString(TAB_TITLES[position]);
  }

  @Override
  public int getCount() {
    return TAB_TITLES.length;
  }
}