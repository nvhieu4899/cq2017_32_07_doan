package com.ygaps.travelapp.TourActivities;

import androidx.fragment.app.FragmentActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.ygaps.travelapp.MyApplication;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.retrofit.request.AddStopPointRequest;
import com.ygaps.travelapp.data.model.Coordinate;
import com.ygaps.travelapp.data.model.StopPoint;
import com.ygaps.travelapp.data.model.TourInfo;
import com.ygaps.travelapp.retrofit.RetrofitClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.textfield.TextInputLayout;
import com.ygaps.travelapp.retrofit.request.GetOneSuggestSP;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ygaps.travelapp.TourActivities.PickSourceAndDestination.bitmapDescriptorFromVector;

public class AddStopPointActivity extends FragmentActivity implements OnMapReadyCallback {

    private ArrayList<StopPoint> pickedSP = new ArrayList<StopPoint>();
    private ArrayList<Marker> pickedSP_marker = new ArrayList<>();
    private ArrayList<StopPoint> suggestSP = new ArrayList<>();
    private TourInfo tourInfo;
    private GoogleMap mMap;
    private int tourId;
    private ArrayList<Integer> deleteIds = new ArrayList<>();
    private String spName = "";
    private String spaddress ="";
    private int spservicesType=1;
    private int spprovinceId=1;
    private int minCost = 0;
    private int maxCost = 0;
    private Button doneBtn;
    private boolean isEditable;
    private boolean isUpdate;
    private boolean isModified = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_stop_point);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Intent intent = getIntent();
        tourId = intent.getIntExtra("tourId",2800);
        isEditable = intent.getBooleanExtra("isEditable",true);
        doneBtn = (Button) findViewById(R.id.btndone);
        doneBtn.setOnClickListener(new OnClickListener() {
            @Override
                public void onClick(View v) {
                if (isEditable&&isModified)
                {
                if (pickedSP.size()>=2) {
                    Call call = RetrofitClient.getInstance()
                            .getApi()
                            .addStopPointToTour(MyApplication.token, new AddStopPointRequest(tourId, pickedSP, deleteIds));
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), "Thành công", Toast.LENGTH_LONG).show();
                                finish();
                            } else {
                                try {
                                    Toast.makeText(getApplicationContext(), response.errorBody().string(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Unable to connect to server", Toast.LENGTH_LONG).show();
                            finish();
                        }
                    });
                }else
                {
                    Toast.makeText(getApplicationContext(),"Cần ít nhất 2 điểm dừng trong tour",Toast.LENGTH_LONG).show();

                }
                }
                else
                {
                    finish();
                }
            }
        });
        Button getSuggestedbtn = (Button)findViewById(R.id.btn_get_suggest);
        getSuggestedbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LatLng latLng = mMap.getCameraPosition().target;
                Coordinate coordinate = new Coordinate(latLng.latitude,latLng.longitude);
                Call call = RetrofitClient.getInstance().getApi().getSuggestedDestinationListOne(MyApplication.token,new GetOneSuggestSP(coordinate));
                call.enqueue(new Callback<StopPoint>() {
                    @Override
                    public void onResponse(Call<StopPoint> call, Response<StopPoint> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getLongtitude()!=null&&response.body().getLatitude()!=null)
                            suggestSP.add(response.body());
                            for (StopPoint sp : suggestSP)
                            {
                                addSPMarker(sp);
                            }

                        }
                        else
                        {
                            try {
                                Toast.makeText(getApplicationContext(),response.errorBody().string(),Toast.LENGTH_LONG).show();

                            }
                            catch (IOException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call call, Throwable t) {
                        Toast.makeText(getApplicationContext(),"Failed to connect to server",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Intent intent = getIntent();
        Call call = RetrofitClient.getInstance().getApi().getTourInfo(MyApplication.token,tourId);
        call.enqueue(new Callback<TourInfo>() {
            @Override
            public void onResponse(Call<TourInfo> call, Response<TourInfo> response) {
                if (response.isSuccessful())
                tourInfo=response.body();
                else
                {
                    try {
                        Toast.makeText(getApplicationContext(),response.errorBody().string(),Toast.LENGTH_LONG).show();

                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<TourInfo> call, Throwable t) {

            }
        });
        ArrayList<LatLng> stopPoint = new ArrayList<LatLng>();

        if (intent.hasExtra("startPointLat"))
        {
        double startLat = intent.getDoubleExtra("startPointLat", 10.762);
        double startLng = intent.getDoubleExtra("startPointLong", 106.660172);
        double endLat = intent.getDoubleExtra("endPointLat", 21.028511);
        double endLng = intent.getDoubleExtra("endPointLong", 105.804817);

        mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(startLat,startLng))
                    .title(getString(R.string.marker_startP))
                    .draggable(false).icon(
                            bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_startpointmarker)
                    ));
        mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(endLat,endLng))
                    .title(getString(R.string.marker_endP))
                    .draggable(false).icon(
                            bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_endpointmarker)
                    ));
        stopPoint.add(new LatLng(startLat, startLng));
        stopPoint.add(new LatLng(endLat, endLng));
        }

        Call call1 = RetrofitClient.getInstance().getApi().getTourInfo(MyApplication.token,tourId);
        call1.enqueue(new Callback<TourInfo>(){
            @Override
            public void onResponse(Call<TourInfo> call, Response<TourInfo> response) {

                if (response.isSuccessful())
                {
                    pickedSP = response.body().getStopPoints();
                    for (StopPoint sp : pickedSP)
                    {
                        addSPMarker(sp);
                    }
                }
                else
                {
                    try {
                        Toast.makeText(getApplicationContext(),response.errorBody().string(),Toast.LENGTH_LONG).show();

                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<TourInfo> call, Throwable t) {

            }
        });
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.18);
        if (stopPoint.size()>0)
        {

            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (LatLng latLng : stopPoint) {
                builder.include(latLng);
            }

            LatLngBounds latLngBounds = builder.build();
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, width, height, padding));

            }
        else
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(10.762622,106.660172),8));



        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                StopPoint stopPoint1 = (StopPoint)marker.getTag();
                if (stopPoint1!=null)
                {
                    if (pickedSP.contains(stopPoint1))
                    displaySPInfoDialog(marker);
                    else displayAddSPDialog(marker.getPosition());
                }
                else {
                    displayAddSPDialog(marker.getPosition());
                }
                return false;
            }
        });
        if (isEditable)
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                displayAddSPDialog(latLng);
            }
        });
    }

    private void addSPMarker(StopPoint sp)
    {
        Marker marker;
        if (sp.getServiceTypeId()==1)
            marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(sp.getLatitude()),Double.parseDouble(sp.getLongtitude())))
                    .title(sp.getName())
                    .draggable(false).icon(
                            bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_restaurant)
                    ));
        else if (sp.getServiceTypeId()==2)
            marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(sp.getLatitude()),Double.parseDouble(sp.getLongtitude())))
                    .title(sp.getName())
                    .draggable(false).icon(
                            bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_hotel)
                    ));
        else if (sp.getServiceTypeId()==3)
            marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(sp.getLatitude()),Double.parseDouble(sp.getLongtitude())))
                    .title(sp.getName())
                    .draggable(false).icon(
                            bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_fuel)
                    ));
        else
            marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(sp.getLatitude()),Double.parseDouble(sp.getLongtitude())))
                    .title(sp.getName())
                    .draggable(false).icon(
                            bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_pin)
                    ));
        marker.setTag(sp);
    }

    public void displaySPInfoDialog(final Marker marker)
    {
        final StopPoint stopPoint = (StopPoint)marker.getTag();
        long arrTime = Long.parseLong(stopPoint.getArrivalTime());
        long leaTime = Long.parseLong(stopPoint.getLeaveTime());
        final Calendar spArrival = new GregorianCalendar();
        spArrival.setTimeInMillis(arrTime);
        final Calendar spLeave = new GregorianCalendar();
        spLeave.setTimeInMillis(leaTime);
        final SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm");
        final SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd/MM/yyyy");

        LayoutInflater layoutInflater = getLayoutInflater();
        final View dialogSpInfo = layoutInflater.inflate(R.layout.info_stop_point_dialog,null);
        final TextInputLayout name = (TextInputLayout) dialogSpInfo.findViewById(R.id.name);
        final TextInputLayout address = (TextInputLayout) dialogSpInfo.findViewById(R.id.address);


        name.getEditText().setText(stopPoint.getName());
        address.getEditText().setText(stopPoint.getAddress());
        final TextView arrivalTime = (TextView)dialogSpInfo.findViewById(R.id.arrivalTime);
        arrivalTime.setText(simpleDateFormat1.format(spArrival.getTime()));

        arrivalTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hour = spArrival.get(Calendar.HOUR_OF_DAY);
                int min = spArrival.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(AddStopPointActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        spArrival.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        spArrival.set(Calendar.MINUTE,minute);
                        arrivalTime.setText(simpleDateFormat1.format(spArrival.getTime()));
                    }
                },hour,min,true);
                timePickerDialog.setTitle(getString(R.string.pickArrivaltime));
                timePickerDialog.show();
            }
        });
        final String[] provinceIds = getResources().getStringArray(R.array.proviceId);
        final TextView arrivalDate = (TextView)dialogSpInfo.findViewById(R.id.arrivalDate);
        arrivalDate.setText(simpleDateFormat2.format(spArrival.getTime()));
        arrivalDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = spArrival.get(Calendar.DAY_OF_MONTH);
                int month = spArrival.get(Calendar.MONTH);
                int year = spArrival.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AddStopPointActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        spArrival.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                        spArrival.set(Calendar.YEAR,year);
                        spArrival.set(Calendar.MONTH,month);
                        arrivalDate.setText(simpleDateFormat2.format(spArrival.getTime()));

                    }
                },year,month,day);
                datePickerDialog.setTitle(getString(R.string.pickArrivalDate));
                datePickerDialog.show();
            }
        });
        final TextView leaveTime = (TextView)dialogSpInfo.findViewById(R.id.leaveTime);
        leaveTime.setText(simpleDateFormat1.format(spLeave.getTime()));

        leaveTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hour = spLeave.get(Calendar.HOUR_OF_DAY);
                int min = spLeave.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(AddStopPointActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        spLeave.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        spLeave.set(Calendar.MINUTE,minute);
                        leaveTime.setText(simpleDateFormat1.format(spLeave.getTime()));

                    }
                },hour,min,true);
                timePickerDialog.setTitle(getString(R.string.leaveTime));
                timePickerDialog.show();
            }
        });

        final TextView leaveDate = (TextView)dialogSpInfo.findViewById(R.id.leaveDate);
        leaveDate.setText(simpleDateFormat2.format(spLeave.getTime()));

        leaveDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = spLeave.get(Calendar.DAY_OF_MONTH);
                int month = spLeave.get(Calendar.MONTH);
                int year = spLeave.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AddStopPointActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        spLeave.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                        spLeave.set(Calendar.YEAR,year);
                        spLeave.set(Calendar.MONTH,month);
                        leaveDate.setText(simpleDateFormat2.format(spLeave.getTime()));
                    }
                },year,month,day);
                datePickerDialog.setTitle(getString(R.string.pickLeaveDate));
                datePickerDialog.show();
            }
        });
        final Spinner services = (Spinner) dialogSpInfo.findViewById(R.id.servicesTypePick);
        String[] servicestype = {getString(R.string.restaurant),getString(R.string.hotel),getString(R.string.reststation),getString(R.string.other)};

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(AddStopPointActivity.this,android.R.layout.simple_spinner_dropdown_item,servicestype);
        services.setAdapter(spinnerAdapter);
        services.setSelection(stopPoint.getServiceTypeId()-1);

        final Spinner province = (Spinner)dialogSpInfo.findViewById(R.id.provincePick);
        ArrayAdapter<String> provincespinneradapter = new ArrayAdapter<String>(AddStopPointActivity.this
                ,android.R.layout.simple_spinner_dropdown_item,provinceIds);
        province.setAdapter(provincespinneradapter);
        province.setSelection(stopPoint.getProvinceId()-1);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(dialogSpInfo);
        final TextInputLayout min = (TextInputLayout)dialogSpInfo.findViewById(R.id.mincost);
        final TextInputLayout max = (TextInputLayout)dialogSpInfo.findViewById(R.id.maxcost);
        min.getEditText().setText(stopPoint.getMinCost());
        max.getEditText().setText(stopPoint.getMaxCost());
        if (isEditable)
        alert.setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                stopPoint.setMaxCost(max.getEditText().getText().toString());
                stopPoint.setMinCost(min.getEditText().getText().toString());
                stopPoint.setName(name.getEditText().getText().toString());
                stopPoint.setArrivalTime(String.valueOf(spArrival.getTime().getTime()));
                stopPoint.setLeaveTime(String.valueOf(spLeave.getTime().getTime()));
                stopPoint.setProvinceId(province.getSelectedItemPosition()+1);
                stopPoint.setServiceTypeId(services.getSelectedItemPosition()+1);
                switch (services.getSelectedItemPosition())
                {
                    case 0:
                        marker.setIcon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_restaurant));
                        break;
                    case 1:
                        marker.setIcon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_hotel));
                        break;
                    case 2:
                        marker.setIcon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_fuel));
                        break;
                    case 3:
                        marker.setIcon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_pin));
                        break;
                }
                isModified = true;
            }
        });
        alert.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        if (isEditable)
        alert.setNeutralButton(R.string.btn_delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    try {
                        deleteIds.add(stopPoint.getId());
                    } catch (NullPointerException e)
                    {
                        e.printStackTrace();
                    }

                    pickedSP_marker.remove(marker);
                    marker.remove();
                    isModified = true;
            }
        });
        final AlertDialog dialog = alert.create();
        alert.show();
    }





    public void displayAddSPDialog(final LatLng latLng)
    {
        final Calendar spLeave =Calendar.getInstance();
        final Calendar spArrival = Calendar.getInstance();
        String[] servicestype = {getString(R.string.restaurant),getString(R.string.hotel),getString(R.string.reststation),getString(R.string.other)};
        final String[] provinceIds = getResources().getStringArray(R.array.proviceId);

        LayoutInflater layoutInflater = getLayoutInflater();
        final View dialogAddSp = layoutInflater.inflate(R.layout.add_stop_point_dialog,null);
        final TextView arrivalTime = (TextView)dialogAddSp.findViewById(R.id.arrivalTime);
        arrivalTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hour = spArrival.get(Calendar.HOUR_OF_DAY);
                int min = spArrival.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(AddStopPointActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
                        spArrival.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        spArrival.set(Calendar.MINUTE,minute);
                        arrivalTime.setText(simpleDateFormat.format(spArrival.getTime()));

                    }
                },hour,min,true);
                timePickerDialog.setTitle(getString(R.string.pickArrivaltime));
                timePickerDialog.show();
            }
        });
        final TextView arrivalDate = (TextView)dialogAddSp.findViewById(R.id.arrivalDate);
        arrivalDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = spArrival.get(Calendar.DAY_OF_MONTH);
                int month = spArrival.get(Calendar.MONTH);
                int year = spArrival.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AddStopPointActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        spArrival.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                        spArrival.set(Calendar.YEAR,year);
                        spArrival.set(Calendar.MONTH,month);
                        arrivalDate.setText(simpleDateFormat.format(spArrival.getTime()));

                    }
                },year,month,day);
                datePickerDialog.setTitle(getString(R.string.pickArrivalDate));
                datePickerDialog.show();
            }
        });
        final TextView leaveTime = (TextView)dialogAddSp.findViewById(R.id.leaveTime);
        leaveTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hour = spLeave.get(Calendar.HOUR_OF_DAY);
                int min = spLeave.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(AddStopPointActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
                        spLeave.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        spLeave.set(Calendar.MINUTE,minute);
                        leaveTime.setText(simpleDateFormat.format(spLeave.getTime()));

                    }
                },hour,min,true);
                timePickerDialog.setTitle(getString(R.string.leaveTime));
                timePickerDialog.show();
            }
        });

        final TextView leaveDate = (TextView)dialogAddSp.findViewById(R.id.leaveDate);
        leaveDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = spLeave.get(Calendar.DAY_OF_MONTH);
                int month = spLeave.get(Calendar.MONTH);
                int year = spLeave.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AddStopPointActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        spLeave.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                        spLeave.set(Calendar.YEAR,year);
                        spLeave.set(Calendar.MONTH,month);
                        leaveDate.setText(simpleDateFormat.format(spLeave.getTime()));
                    }
                },year,month,day);
                datePickerDialog.setTitle(getString(R.string.pickLeaveDate));
                datePickerDialog.show();
            }
        });
        final TextInputLayout name = (TextInputLayout) dialogAddSp.findViewById(R.id.name);
        final TextInputLayout address = (TextInputLayout) dialogAddSp.findViewById(R.id.address);
        try {
            address.getEditText().setText(CreateTour.getFullAddressStr(getAddressFromLatLng(latLng)));
        }
        catch (NullPointerException e) {
            e.printStackTrace();
            address.getEditText().setText("");
            address.setError(getString(R.string.error_add));
        }

        final Spinner services = (Spinner) dialogAddSp.findViewById(R.id.servicesTypePick);

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(AddStopPointActivity.this,android.R.layout.simple_spinner_dropdown_item,servicestype);
        services.setAdapter(spinnerAdapter);

        final Spinner province = (Spinner)dialogAddSp.findViewById(R.id.provincePick);
        ArrayAdapter<String> provincespinneradapter = new ArrayAdapter<String>(AddStopPointActivity.this
                ,android.R.layout.simple_spinner_dropdown_item,provinceIds);
        province.setAdapter(provincespinneradapter);
         AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(dialogAddSp);
        final TextInputLayout min = (TextInputLayout)dialogAddSp.findViewById(R.id.mincost);
        final TextInputLayout max = (TextInputLayout)dialogAddSp.findViewById(R.id.maxcost);
        min.getEditText().setText("0");
        max.getEditText().setText("0");
        alert.setPositiveButton(R.string.btn_addSP, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                spName = name.getEditText().getText().toString();
                spaddress = address.getEditText().getText().toString();

                minCost = Integer.parseInt(min.getEditText().getText().toString());
                maxCost = Integer.parseInt(max.getEditText().getText().toString());
                spservicesType = services.getSelectedItemPosition()+1;
                spprovinceId=province.getSelectedItemPosition()+1;
                StopPoint sp = new StopPoint(spName
                        ,String.valueOf(latLng.latitude)
                        ,String.valueOf(latLng.longitude)
                        ,String.valueOf(spArrival.getTimeInMillis())
                        ,String.valueOf(spLeave.getTimeInMillis())
                        ,spaddress
                        ,String.valueOf(minCost)
                        ,String.valueOf(maxCost)
                        ,spservicesType,null,spprovinceId);
                pickedSP.add(sp);
                addSPMarker(sp);
                isModified = true;
            }
        });
        alert.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        final AlertDialog dialog = alert.create();
        alert.show();
    }
    private List<Address> getAddressFromLatLng(LatLng latLng) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses.isEmpty())
                return null;// Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
            return addresses;
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Không thể lấy dịa chỉ", Toast.LENGTH_LONG).show();
            return null;
        }
    }
    @Override
    public void onBackPressed() {
        if (isModified) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.discardTitle));
            builder.setMessage(getString(R.string.discardMessage));

            builder.setPositiveButton(getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    AddStopPointActivity.super.onBackPressed();
                }
            });
            builder.setNegativeButton(getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
        else
        super.onBackPressed();
    }
}
